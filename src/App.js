import React, { Fragment, useEffect, lazy, Suspense } from "react";
import { BrowserRouter, HashRouter, Route, Switch } from "react-router-dom";
// Redux
import { Provider } from "react-redux";
import store from "./store/store";
import { LOGOUT } from "./actions/types";
import { loadUser } from "./actions/authAction";
import setAuthToken from "./utils/setAuthToken";
import { ToastContainer } from "react-toastify";
import ViewLoader from "./Components/ViewLoader";
import Spinner from "./containers/layout/Spinner";
import { io } from "socket.io-client";

let baseURL;

if (window.location.hostname == "localhost") {
  baseURL = "https://localhost:5024";
} else if (window.location.hostname == "dev74.onlinetestingserver.com") {
  baseURL = "https://dev74.onlinetestingserver.com:5024";
}

export const socket = io(baseURL);
const ForgotPassword = lazy(() => import("./containers/auth/ForgotPassword"));
const forgotCode = lazy(() => import("./containers/auth/ForgotCode"));
const ResetPassword = lazy(() => import("./containers/auth/ResetPassword"));
const Login = lazy(() => import("./containers/auth/Login"));
const Header = lazy(() => import("./containers/dashboard/Header"));
const Footer = lazy(() => import("./containers/dashboard/Footer"));
const Routes = lazy(() => import("./containers/routing/Routes"));

const App = () => {
  useEffect(() => {
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    store.dispatch(loadUser());
    const socket = io(baseURL);
    // log user out from all tabs if they log out in one tab
    window.addEventListener("storage", () => {
      if (!localStorage.token) store.dispatch({ type: LOGOUT });
    });
  }, []);

  return (
    <Fragment>
      <ToastContainer autoClose={3000} />

      <Provider store={store}>
        {/* <BrowserRouter basename="/bags-away/admin/"> */}
        <BrowserRouter basename="/admin/">

          {/* <HashRouter> */}
          <Suspense
            fallback={
              <>
                <ViewLoader fontSize="100px" />
              </>
            }
          >
            <Fragment>
              <Header />
              <Switch>
                <Route
                  exact
                  path="/forgotpassword"
                  component={ForgotPassword}
                />
                <Route exact path="/forgotcode" component={forgotCode} />
                <Route exact path="/resetpassword" component={ResetPassword} />
                <Route exact path="/" component={Login} />

                <Route component={Routes} />
              </Switch>
            </Fragment>
            <Footer />
          </Suspense>
          {/* </HashRouter> */}
        </BrowserRouter>
      </Provider>
    </Fragment>
  );
};

export default App;
