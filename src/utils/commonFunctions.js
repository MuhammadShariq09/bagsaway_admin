
// import {MarkNotificationReadTrue} from '../actions/profileAction'
module.exports = {
    getSerial :(per_page,current_page,index=0)=>{

        let starting = per_page * (current_page - 1);
        index++;
        return starting + index;
    },
    get_ID:(id)=>{
      let reduce_id =   id.substr(id.length - 4)
        return reduce_id
    },
     handlenotifcation :  (item,history) => {
        console.log(item)
        // console.log(item?.payload?.type)
        // MarkNotificationReadTrue(item._id);
    
        if (item?.payload?.type == "user") {
          history.push(`/users/${item?.payload?.id}`);
        } else if (item?.payload?.type == "delivery") {
          history.push(`/delivery-detail/${item?.payload?.id}`);
        } else if (item?.payload?.type == "VERIFICATION") {
          history.push(`/verifications/${item?.payload?.id}`);
        } else {
          history.push(`/notifications`);
        }
    }
     

}