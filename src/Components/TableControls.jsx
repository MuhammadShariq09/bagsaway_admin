import React from "react";
import DatePicker from "react-datepicker";

export default function TableControls({
 handleLimit,handleKeyword
}) {
  return (
    <>
      <div className="row">
        <div className="col-sm-12 col-md-6">
          <div className="dataTables_length" id="DataTables_Table_0_length">
            <label>
              Show{" "}
              <select
                name="DataTables_Table_0_length"
                aria-controls="DataTables_Table_0"
                className="form-control form-control-sm"
                onChange={(e) => handleLimit(e)}
              >
                <option value={10}>10</option>
                <option value={25}>25</option>
                <option value={50}>50</option>
                <option value={100}>100</option>
              </select>{" "}
              entries
            </label>
          </div>
        </div>
        <div className="col-sm-12 col-md-6">
          <div id="DataTables_Table_0_filter" className="dataTables_filter">
            <label>
              Search:
              <input
                onChange={(e) => handleKeyword(e)}
                spellCheck="true"
                type="search"
                className="form-control form-control-sm"
                placeholder="Search"
                aria-controls="DataTables_Table_0"
              />
            </label>
          </div>
        </div>
      </div>
    </>
  );
}

//  {/* <div className="row mt-1" style={{ width: "100%" }}>
//         <div className="col-lg-7 col-12">
//           <div className="d-flex align-items-center">
//             <p className="l-grey source f-20 d-lg-inline-block">Sort By:</p>
//             <div className="ml-2">
//               {/* <p class="l-grey source mb-1">From</p> */}
//               <div
//                 role="wrapper"
//                 className="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group"
//               >
//                 <DatePicker
//                   selected={from}
//                   onChange={(date) => setFrom(date)}
//                   className="sort-date customdate form-control"
//                 />
//               </div>
//             </div>
//             <div className="ml-2">
//               {/* <p class="l-grey source mb-1">To</p> */}
//               <div
//                 role="wrapper"
//                 className="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group"
//               >
//                 <DatePicker
//                   selected={to}
//                   onChange={(date) => setTo(date)}
//                   className="sort-date customdate form-control"
//                 />
//               </div>
//             </div>
//           </div>
//         </div>
//         <div className="offset-lg-2 col-lg-3 col-12 userss">
//           {!hide_status && (
//             <select
//               value={status}
//               onChange={(e) => setStatus(e.target.value)}
//               id
//               className="form-control"
//             >
//               <option value="">Select Status</option>
//               {status_options?.length > 0 ? (
//                 status_options?.map((status) => (
//                   <option value={status}>{status}</option>
//                 ))
//               ) : (
//                 <>
//                   <option value={true}>Active</option>
//                   <option value={false}>Inactive</option>
//                 </>
//               )}
//             </select>
//           )}
//         </div>
//       </div>
//       <div className="maain-tabble table-responsive overflow-hidden">
//         <div
//           id="DataTables_Table_0_wrapper"
//           className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
//         >
//           <div className="row">
//             <div className="col-sm-12 col-md-6">
//               <div className="dataTables_length" id="DataTables_Table_0_length">
//                 <label>
//                   Show{" "}
//                   <select
//                     name="DataTables_Table_0_length"
//                     aria-controls="DataTables_Table_0"
//                     className="form-control form-control-sm"
//                     value={perPage}
//                     onChange={(e) => setPerPage(e.target.value)}
//                   >
//                     <option value={10}>10</option>
//                     <option value={25}>25</option>
//                     <option value={50}>50</option>
//                     <option value={100}>100</option>
//                   </select>{" "}
//                   entries
//                 </label>
//               </div>
//             </div>
//             <div className="col-sm-12 col-md-6">
//               <div id="DataTables_Table_0_filter" className="dataTables_filter">
//                 <label>
//                   Search:
//                   <input
//                     type="search"
//                     className="form-control form-control-sm"
//                     placeholder
//                     aria-controls="DataTables_Table_0"
//                     value={searchString}
//                     onChange={(e) => setSearchString(e.target.value)}
//                     onKeyDown={(e) => {
//                       if (e.key === "Enter") {
//                         searchFunction();
//                       }
//                     }}
//                   />
//                 </label>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>

// <div className="app-content content view user">
// <div className="content-wrapper">
//   <div className="content-body">
//     {/* Basic form layout section start */}
//     <section id="configuration" className="search view-cause">
//       <div className="row">
//         <div className="col-12">
//           <div className="card pad-20">
//             <div className="card-content collapse show">
//               <div className="card-body table-responsive card-dashboard">
//                 <div className="row mt-2">
//                   <div className="col-12 d-block d-sm-flex justify-content-between">
//                     <div className="left">
//                       <h1>Deliveries</h1>
//                     </div>
//                     <div className="right text-right">
//                       <div className="all-select">
//                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                         &nbsp;
//                         <select name id className="form-control filter">
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp;
//                           <option value>Filter By Status</option>
//                           <option value>All</option>
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp;
//                           <option value>Pending</option>
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp;
//                           <option value>Approved</option>
//                           <option value>Rejected</option>
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp;
//                           <option value>Delivered</option>
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                           &nbsp; &nbsp; &nbsp;
//                         </select>
//                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//                 <div className="row maain-tabble mt-1">
//                   <div
//                     id="DataTables_Table_0_wrapper"
//                     className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
//                   >
//                <TableControls/>
//                     <div className="row">
//                       <div className="col-sm-12">
//                         <table
//                           className="table table-striped table-bordered zero-configuration dataTable no-footer"
//                           id="DataTables_Table_0"
//                           role="grid"
//                           aria-describedby="DataTables_Table_0_info"
//                         >
//                           <thead>
//                             <tr role="row">
//                               <th
//                                 className="sorting_asc"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-sort="ascending"
//                                 aria-label="S.No: activate to sort column descending"
//                                 style={{ width: "44.625px" }}
//                               >
//                                 S.No
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="ID No: activate to sort column ascending"
//                                 style={{ width: "56.5156px" }}
//                               >
//                                 ID No
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="First Name: activate to sort column ascending"
//                                 style={{ width: "97.5156px" }}
//                               >
//                                 First Name
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Last Name: activate to sort column ascending"
//                                 style={{ width: "93.2031px" }}
//                               >
//                                 Last Name
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Email Address: activate to sort column ascending"
//                                 style={{ width: "138.141px" }}
//                               >
//                                 Email Address
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Status: activate to sort column ascending"
//                                 style={{ width: "68.5156px" }}
//                               >
//                                 Status
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Total Cost: activate to sort column ascending"
//                                 style={{ width: "102.797px" }}
//                               >
//                                 Total Cost
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Fed-Ex Cost: activate to sort column ascending"
//                                 style={{ width: "110.125px" }}
//                               >
//                                 Fed-Ex Cost
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Date: activate to sort column ascending"
//                                 style={{ width: "101.375px" }}
//                               >
//                                 Date
//                               </th>
//                               <th
//                                 className="sorting"
//                                 tabIndex={0}
//                                 aria-controls="DataTables_Table_0"
//                                 rowSpan={1}
//                                 colSpan={1}
//                                 aria-label="Action: activate to sort column ascending"
//                                 style={{ width: "67.8125px" }}
//                               >
//                                 Action
//                               </th>
//                             </tr>
//                           </thead>
//                           <tbody>
//                             <tr role="row" className="odd">
//                               <td className="sorting_1">01</td>
//                               <td>Bag001</td>
//                               <td>Rony</td>
//                               <td>Weasley</td>
//                               <td>email@email.com</td>
//                               <td className="fc-green">Active</td>
//                               <td>$3000</td>
//                               <td>$100</td>
//                               <td>mm/dd/yyyy</td>
//                               <td>
//                                 <div className="btn-group mr-1 mb-1">
//                                   <button
//                                     type="button"
//                                     className="btn  btn-drop-table btn-sm"
//                                     data-toggle="dropdown"
//                                     aria-haspopup="true"
//                                     aria-expanded="false"
//                                   >
//                                     {" "}
//                                     <i className="fa fa-ellipsis-v" />
//                                   </button>
//                                   <div
//                                     className="dropdown-menu dropdown-menu-right"
//                                     x-placement="bottom-start"
//                                     style={{
//                                       position: "absolute",
//                                       transform:
//                                         "translate3d(4px, 23px, 0px)",
//                                       top: "0px",
//                                       left: "0px",
//                                       willChange: "transform",
//                                     }}
//                                   >
//                                     <a
//                                       className="dropdown-item"
//                                       href="delivery-details.php"
//                                     >
//                                       <i className="fa fa-eye" />
//                                       Details
//                                     </a>
//                                   </div>
//                                 </div>
//                               </td>
//                             </tr>
//                             <tr role="row" className="even">
//                               <td className="sorting_1">01</td>
//                               <td>Bag001</td>
//                               <td>Rony</td>
//                               <td>Weasley</td>
//                               <td>email@email.com</td>
//                               <td className="fc-yellow">Pending</td>
//                               <td>$3000</td>
//                               <td>$100</td>
//                               <td>mm/dd/yyyy</td>
//                               <td>
//                                 <div className="btn-group mr-1 mb-1">
//                                   <button
//                                     type="button"
//                                     className="btn  btn-drop-table btn-sm"
//                                     data-toggle="dropdown"
//                                     aria-haspopup="true"
//                                     aria-expanded="false"
//                                   >
//                                     {" "}
//                                     <i className="fa fa-ellipsis-v" />
//                                   </button>
//                                   <div
//                                     className="dropdown-menu dropdown-menu-right"
//                                     x-placement="bottom-start"
//                                     style={{
//                                       position: "absolute",
//                                       transform:
//                                         "translate3d(4px, 23px, 0px)",
//                                       top: "0px",
//                                       left: "0px",
//                                       willChange: "transform",
//                                     }}
//                                   >
//                                     <a
//                                       className="dropdown-item"
//                                       href="delivery-details.php"
//                                     >
//                                       <i className="fa fa-eye" />
//                                       Details
//                                     </a>
//                                   </div>
//                                 </div>
//                               </td>
//                             </tr>
//                             <tr role="row" className="odd">
//                               <td className="sorting_1">01</td>
//                               <td>Bag001</td>
//                               <td>Rony</td>
//                               <td>Weasley</td>
//                               <td>email@email.com</td>
//                               <td className="fc-red">Rejected</td>
//                               <td>$3000</td>
//                               <td>$100</td>
//                               <td>mm/dd/yyyy</td>
//                               <td>
//                                 <div className="btn-group mr-1 mb-1">
//                                   <button
//                                     type="button"
//                                     className="btn  btn-drop-table btn-sm"
//                                     data-toggle="dropdown"
//                                     aria-haspopup="true"
//                                     aria-expanded="false"
//                                   >
//                                     {" "}
//                                     <i className="fa fa-ellipsis-v" />
//                                   </button>
//                                   <div
//                                     className="dropdown-menu dropdown-menu-right"
//                                     x-placement="bottom-start"
//                                     style={{
//                                       position: "absolute",
//                                       transform:
//                                         "translate3d(4px, 23px, 0px)",
//                                       top: "0px",
//                                       left: "0px",
//                                       willChange: "transform",
//                                     }}
//                                   >
//                                     <a
//                                       className="dropdown-item"
//                                       href="delivery-rejected.php"
//                                     >
//                                       <i className="fa fa-eye" />
//                                       Details
//                                     </a>
//                                   </div>
//                                 </div>
//                               </td>
//                             </tr>
//                           </tbody>
//                         </table>
//                       </div>
//                     </div>
//                     <div className="row">
//                       <div className="col-sm-12 col-md-5">
//                         <div
//                           className="dataTables_info"
//                           id="DataTables_Table_0_info"
//                           role="status"
//                           aria-live="polite"
//                         >
//                           Showing 1 to 3 of 3 entries
//                         </div>
//                       </div>
//                       <div className="col-sm-12 col-md-7">
//                         <div
//                           className="dataTables_paginate paging_simple_numbers"
//                           id="DataTables_Table_0_paginate"
//                         >
//                           <ul className="pagination">
//                             <li
//                               className="paginate_button page-item previous disabled"
//                               id="DataTables_Table_0_previous"
//                             >
//                               <a
//                                 href="#"
//                                 aria-controls="DataTables_Table_0"
//                                 data-dt-idx={0}
//                                 tabIndex={0}
//                                 className="page-link"
//                               >
//                                 Previous
//                               </a>
//                             </li>
//                             <li className="paginate_button page-item active">
//                               <a
//                                 href="#"
//                                 aria-controls="DataTables_Table_0"
//                                 data-dt-idx={1}
//                                 tabIndex={0}
//                                 className="page-link"
//                               >
//                                 1
//                               </a>
//                             </li>
//                             <li
//                               className="paginate_button page-item next disabled"
//                               id="DataTables_Table_0_next"
//                             >
//                               <a
//                                 href="#"
//                                 aria-controls="DataTables_Table_0"
//                                 data-dt-idx={2}
//                                 tabIndex={0}
//                                 className="page-link"
//                               >
//                                 Next
//                               </a>
//                             </li>
//                           </ul>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   </div>
// </div>
// </div>
