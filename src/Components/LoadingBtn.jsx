import React from "react";

export default function LoadingBtn({ className }) {
  return (
    <button className={className}>
      <i className="fas fa-circle-notch fa-spin"></i> Loading
    </button>
  );
}
