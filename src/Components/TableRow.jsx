import React from "react";

export default function TableRow({ data, children }) {
  return data?.map((dat, index) => {
    if (dat == "action") {
      return children;
    } else return <td key={index}>{dat}</td>;
  });
}
