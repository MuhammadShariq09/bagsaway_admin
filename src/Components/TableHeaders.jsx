import React from "react";

export default function TableHeaders({ data }) {
  return (
    <thead>
      <tr role="row">
        {data?.map((heading, index) => (
          <th rowSpan={1} colSpan={1} style={{ width: "auto" }} key={index}>
            {heading}
          </th>
        ))}
      </tr>
    </thead>
  );
}
