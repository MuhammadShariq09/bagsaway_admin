import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getCount } from "../Redux";
import { useEffect } from "react";
import LoadingIcon from "./LoadingIcon";

export const Header = ({ getCount, count, loading, user }) => {
  useEffect(() => {
    getCount();
  }, []);

  return (
    <nav className="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
      <div className="navbar-wrapper">
        <div className="navbar-header">
          <ul className="nav navbar-nav flex-row">
            <li className="nav-item mobile-menu d-md-none mr-auto">
              <Link
                className="nav-link nav-menu-main menu-toggle hidden-xs"
                href="#"
              >
                <i className="ft-menu font-large-1" />
              </Link>
            </li>
            <li className="nav-item mt-1">
              <Link className="navbar-brand" to="/dashboard">
                <img
                  className="brand-logo"
                  alt="stack admin logo"
                  src="images/Logo.png"
                />
              </Link>
            </li>
            <li className="nav-item d-md-none">
              <Link
                className="nav-link open-navbar-container"
                data-toggle="collapse"
                data-target="#navbar-mobile"
              >
                <i className="fa fa-ellipsis-v" />
              </Link>
            </li>
          </ul>
        </div>
        <div className="navbar-container content">
          <div className="collapse navbar-collapse" id="navbar-mobile">
            <ul className="nav navbar-nav mr-auto float-left"></ul>
            <ul className="nav navbar-nav float-right">
              <li
                className="dropdown dropdown-notification nav-item bell-icon"
                id="hoverbell"
              >
                {loading ? (
                  <Link
                    className="nav-link py-0 nav-link-label mt-1"
                    style={{ marginTop: -3 }}
                  >
                    <LoadingIcon />
                  </Link>
                ) : (
                  <Link
                    className="nav-link py-0 nav-link-label mt-1"
                    to="/notification"
                  >
                    <i className="far fa-bell fa-2x" />
                    <span className="notif-counter text-right">{count}</span>
                  </Link>
                )}
              </li>
              <li className="dropdown d-flex align-items-center dropdown-user nav-item">
                <Link
                  className="dropdown-toggle nav-link dropdown-user-link"
                  href="#"
                  data-toggle="dropdown"
                >
                  <span className="avatar avatar-online">
                    <img src="images//user-icon.png" alt="user avatar" />
                  </span>
                  <span className="user-name">
                    {user?.first_name ? user?.first_name : "Admin"}{" "}
                    <i className="fas ml-1 fa-chevron-down" />
                  </span>
                </Link>
                <div className="dropdown-menu dropdown-menu-right">
                  <Link className="dropdown-item" href="#_">
                    <i className="fas fa-user-circle" />
                    Profile
                  </Link>
                  <Link
                    className="dropdown-item"
                    onClick={() => {
                      localStorage.clear();
                      window.location.reload();
                    }}
                  >
                    <i className="fas fa-sign-out-alt" />
                    Logout
                  </Link>
                </div>
              </li>
              <li className="nav-item d-none d-md-block">
                <Link
                  className="nav-link nav-menu-main menu-toggle hidden-xs"
                  href="#"
                >
                  <i className="ft-menu" />
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

const mapStateToProps = (state) => {
  return {
    count: state.notification.count,
    loading: state.notification.loading,
    user: state.auth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCount: () => dispatch(getCount()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
