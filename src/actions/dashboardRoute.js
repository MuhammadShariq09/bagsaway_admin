import api from '../utils/api';
import {  toast } from 'react-toastify';

import {
    DASHBOARD_ERROR,
GET_DASHBOARD_DATA,
GET_DASHBOARD_GRAPHS

} from './types';



// Get all reports
export const getDashboard = (year) => async dispatch => {

  try {
    const totalcounts  =  await api.get(`/dashboard/stats`);
    const  requstspermonth= await api.get(`/dashboard/getanalytics?year=${year}`); 


// console.log()
    dispatch({
      type: GET_DASHBOARD_DATA,
      payload:  {total_counts : totalcounts?.data , total_requests:requstspermonth.data}
    });
  } catch (err) {
    
    dispatch({
      type: DASHBOARD_ERROR,
    //   payload: { msg: err.response.statusText, status: err.response.status }
      
    });
  }
};

