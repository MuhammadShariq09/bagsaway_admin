
import api from '../utils/api';
import {  toast } from 'react-toastify';

import {
    DELIVERYS_ERROR,
  GET_DELIVERYS,
  UPDATE_DELIVERY,
  GET_DELIVERY_BY_ID
} from './types';
;


// Get all Deliveries
export const getDeliveries = (page, limit, selection,from,to,keyword) => async dispatch => {
    // dispatch({ type: CLEAR_PROFILE });

    try {
      dispatch({ type: "START_LOADER",payload : true });

        const res = await api.get(`/deliveries?page=${page}&limit=${limit}&selection=${selection}&from=${from}&to=${to}&keyword=${keyword}`);


        



        dispatch({
            type: GET_DELIVERYS,
            payload: res.data
        });
        dispatch({ type: "START_LOADER",payload : false });

        
    } catch (err) {
      dispatch({ type: "START_LOADER",payload : false });


        dispatch({
            type: DELIVERYS_ERROR,
            payload: err

        });
    }
};



// Get profile by ID
export const getDeliveyByID = delivery_id => async dispatch => {

    try {
        const res = await api.get(`/deliveries/${delivery_id}`);
        // console.log(res)
        dispatch({
            type: GET_DELIVERY_BY_ID,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: DELIVERYS_ERROR,
        });
    }
};



//update Travek status
export const UpdateDeliveryStatus = (delivery_id,status,history) => async dispatch => {
    // console.log(luggerId)
    const body = JSON.stringify({ delivery_id,status})
  
  // console.log(body)
  // console.log("lugger status update called")
    try {
      const res = await api.post(`/deliveries/status`,body)
  
  
      
      console.log(res.data)
      dispatch({
        type: UPDATE_DELIVERY,
        // payload: res.data
        
      });
  
  
  if(res.status==200){
      dispatch(getDeliveyByID(delivery_id))
  
        toast.success(`🦄 ${res.data.message}`, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
  
  
        window.jQuery('#approve').modal('hide');
        window.jQuery('.modal-backdrop').remove();
        window.jQuery('#confirm').modal('show');
        // history.push(`/deliveries/${delivery_id}`)
  
    }
        
    } catch (err) {
      console.log("err.response",err)
    //   toast.error
      dispatch({
        type:DELIVERYS_ERROR ,
        // payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  };
  