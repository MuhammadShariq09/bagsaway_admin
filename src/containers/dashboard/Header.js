import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Link, Redirect, useLocation, withRouter } from "react-router-dom";
import { logout } from "../../actions/authAction";
import { getNotifications } from "../../actions/profileAction";
import { connect } from "react-redux";
import Spinner from "../layout/Spinner";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import Moment from "react-moment";
import { handlenotifcation } from "../../utils/commonFunctions";

const Header = ({
  getNotifications,
  auth: { isAuthenticated, loading, user },
  profile: { Notifications },
  history,
  logout,
}) => {
  const { pathname } = useLocation();
  let [active, setActive] = useState(false);

  let notifications = Notifications;
  // console.log(notifications)
  const [toggle, setToggle] = useState(false);
  const [toggleProduct, setToggleProduct] = useState(false);
  useEffect(() => {
    if (isAuthenticated) {
      getNotifications(1, 5);
    }
  }, [getNotifications, isAuthenticated]);

  const toggleMenu = () => {
    if (!active) {
      document.querySelector("body").classList.remove("menu-expanded");
      document.querySelector("body").classList.add("menu-collapsed");
      setActive(!active);
    } else {
      document.querySelector("body").classList.remove("menu-collapsed");
      document.querySelector("body").classList.add("menu-expanded");
      setActive(!active);
    }
  };
  // const getCount = ()=>{
  //   let count = 0
  //   if(Object.keys(notifications).length>0){

  //     let newNotification  = notifications.map(noti  => noti.isread==false)
  //      count= newNotification.length
  //   }
  //   return count

  // }

  const authLinks = (
    <div>
      <nav className="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
        <div className="navbar-wrapper">
          <div className="navbar-header">
            <ul className="nav navbar-nav flex-row">
              <li className="nav-item mobile-menu d-md-none mr-auto">
                <a
                  className="nav-link nav-menu-main menu-toggle hidden-xs"
                  href="#"
                >
                  <i className="ft-menu font-large-1" />
                </a>
              </li>
              <li className="nav-item">
                {" "}
                <Link className="navbar-brand" to="/dashboard">
                  {" "}
                  <img
                    className="brand-logo"
                    alt="stack admin logo"
                    src="images/logo.png"
                  />{" "}
                </Link>{" "}
              </li>
              <li className="nav-item d-md-none">
                {" "}
                <a
                  className="nav-link open-navbar-container"
                  data-toggle="collapse"
                  data-target="#navbar-mobile"
                >
                  <i className="fa fa-ellipsis-v" />
                </a>{" "}
              </li>
            </ul>
          </div>
          <div className="navbar-container content">
            <div className="collapse navbar-collapse" id="navbar-mobile">
              <ul className="nav navbar-nav mr-auto float-left"></ul>
              <ul className="nav navbar-nav float-right">
                <li className="dropdown dropdown-notification nav-item">
                  {" "}
                  <a
                    className="nav-link nav-link-label"
                    href="#"
                    data-toggle="dropdown"
                  >
                    <i className="fa fa-bell" aria-hidden="true" />
                  </a>
                  <ul className="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li
                      className="scrollable-container media-list ps-container ps-theme-dark ps-active-y"
                      data-ps-id="cbae8718-1b84-97ac-6bfa-47d792d8ad89"
                    >
                      {Object.keys(Notifications).length > 0 ? (
                        Notifications.data.map((notification) => (
                          <div className="media">
                            <i className="fa fa-circle" />
                            <div className="media-body">
                              <p className="notification-text font-small-3 text-muted">
                                {notification.title}
                                <br />
                                <span>
                                  {moment(notification.createdAt).format("LL")}
                                </span>
                              </p>
                            </div>
                            <a
                              onClick={() =>
                                handlenotifcation(notification, history)
                              }
                            >
                              <i className="fa fa-eye" />
                            </a>
                          </div>
                        ))
                      ) : (
                        <div className="media">
                          <i className="fa fa-circle" />
                          <div className="media-body">
                            <p className="notification-text font-small-3 text-muted">
                              No notification found...
                            </p>
                          </div>
                        </div>
                      )}
                    </li>
                    <li className="dropdown-menu-footer">
                      <Link
                        className="dropdown-item text-muted text-center"
                        to="/notifications"
                      >
                        View All
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown dropdown-user nav-item">
                  <a
                    className="dropdown-toggle nav-link dropdown-user-link"
                    href="#"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    {" "}
                    <span className="avatar avatar-online">
                      {" "}
                      <img
                        src={user?.image}
                        alt="avatar"
                        onError={(e) => {
                          e.target.onerror = null;
                          e.target.src = "images/student-pro-girl.png";
                        }}
                      />
                    </span>{" "}
                    <span className="user-name">
                      {user?.firstname + " " + user?.lastname}
                    </span>{" "}
                  </a>
                  <div className="dropdown-menu dropdown-menu-right profile-drp-down">
                    {/* <h3>Account</h3> */}
                    <ul className>
                      <li>
                        <Link className="dropdown-item text-left" to="/profile">
                          <i className="fa fa-user" />
                          My Profile
                        </Link>
                      </li>
                    </ul>
                    <div className="dropdown-divider m-0" />
                    <Link
                      onClick={() => logout()}
                      className="dropdown-item text-left"
                    >
                      <i className="fa fa-power-off" />
                      Logout
                    </Link>
                  </div>
                </li>
                <li className="nav-item d-none d-md-block">
                  <a
                    className="nav-link nav-menu-main menu-toggle hidden-xs"
                    onClick={() => toggleMenu()}
                  >
                    <i className="ft-menu" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      {/* ////////////////////////////////////////////////////////////////////////////*/}
      <div
        className="main-menu menu-fixed menu-light menu-accordion"
        data-scroll-to-active="true"
      >
        <div className="main-menu-content">
          <ul
            className="navigation navigation-main"
            id="main-menu-navigation"
            data-menu="menu-navigation"
          >
            <li
              className={`nav-item ${pathname == "/dashboard" ? "active" : ""}`}
            >
              <Link to="/dashboard">
                <i className="fa fa-home" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Dashboard
                </span>
              </Link>
            </li>
            <li className={`nav-item ${pathname == "/users" ? "active" : ""}`}>
              <Link to="/users">
                <i className="fa fa-user-circle" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Users
                </span>
              </Link>
            </li>
            <li
              className={`nav-item ${
                pathname == "/deliveries" ? "active" : ""
              }`}
            >
              <Link to="/deliveries">
                <i className="fas fa-user-cog" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Deliveries
                </span>
              </Link>
            </li>
            <li
              className={`nav-item ${pathname == "/reports" ? "active" : ""}`}
            >
              <Link to="/reports">
                <i className="fas fa-user-cog" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Report Logs
                </span>
              </Link>
            </li>
            <li className={`nav-item ${pathname == "/chat" ? "active" : ""}`}>
              <Link to="/chat">
                <i className="fas fa-user-cog" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Chat
                </span>
              </Link>
            </li>
            <li
              className={`nav-item ${pathname == "/feedback" ? "active" : ""}`}
            >
              <Link to="/feedback">
                <i className="fas fa-user-cog" aria-hidden="true" />
                <span className="menu-title" data-i18n>
                  Feedbacks
                </span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );

  // if(!isAuthenticated){
  //   return history.push('/login')
  // }

  return (
    <div>
      {!loading && (
        <Fragment>{isAuthenticated ? authLinks : <Redirect to="/" />}</Fragment>
      )}
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  getNotifications: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
});

export default connect(mapStateToProps, { logout, getNotifications })(
  withRouter(Header)
);
