import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "../layout/Spinner";
// import ProfileItem from './ProfileItem';
import ReactPaginate from "react-paginate";
import { getNotifications } from "../../actions/profileAction";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link } from "react-router-dom";
import Pagination from "../../utils/pagination";
import moment from "moment";

const Notifications = ({
  getNotifications,
  profile: { Users, loading, Notifications },
  history,
}) => {
  // const [notifications, setNotification] = useState({});
  //   const [UsersPerPage] = useState(5);
  const [page, setPage] = useState(1);

  useEffect(() => {
    getNotifications(page);
  }, [page]);

  const onPage = (selectedPage) => {
    alert(selectedPage);
    setPage(selectedPage.selected + 1);
  };
  const onPageLoad = () => {
    getNotifications(page);
  };
  const handlePageClick = (data) => {
    console.log(data);
    let selected = data.selected;
    setPage(selected);
  };

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <h3 className="black s-bold mt-1 source p_lg f-30">Activity Log</h3>
          <div className="maain-tabble">
            <div
              id="DataTables_Table_0_wrapper"
              className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
            >
              <div className="row">
                <div className="col-sm-12 col-md-6">
                  <div
                    className="dataTables_length"
                    id="DataTables_Table_0_length"
                  >
                    <label>
                      Show{" "}
                      <select
                        name="DataTables_Table_0_length"
                        aria-controls="DataTables_Table_0"
                        className="form-control form-control-sm"
                      >
                        <option value={10}>10</option>
                        <option value={25}>25</option>
                        <option value={50}>50</option>
                        <option value={100}>100</option>
                      </select>{" "}
                      entries
                    </label>
                  </div>
                </div>
                <div className="col-sm-12 col-md-6">
                  <div
                    id="DataTables_Table_0_filter"
                    className="dataTables_filter"
                  >
                    <label>
                      Search:
                      <input
                        type="search"
                        className="form-control form-control-sm"
                        placeholder
                        aria-controls="DataTables_Table_0"
                      />
                    </label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <table
                    className="table table-striped table-bordered zero-configuration dataTable no-footer"
                    id="DataTables_Table_0"
                    role="grid"
                    aria-describedby="DataTables_Table_0_info"
                  >
                    <thead>
                      <tr role="row">
                        <th
                          className="black bold sorting_asc"
                          tabIndex={0}
                          aria-controls="DataTables_Table_0"
                          rowSpan={1}
                          colSpan={1}
                          aria-sort="ascending"
                          aria-label="Activity: activate to sort column descending"
                          style={{ width: "1083px" }}
                        >
                          Activity
                        </th>
                        <th
                          className="black bold sorting"
                          tabIndex={0}
                          aria-controls="DataTables_Table_0"
                          rowSpan={1}
                          colSpan={1}
                          aria-label="Date: activate to sort column ascending"
                          style={{ width: "226px" }}
                        >
                          Date
                        </th>
                        <th
                          className="black bold sorting"
                          tabIndex={0}
                          aria-controls="DataTables_Table_0"
                          rowSpan={1}
                          colSpan={1}
                          aria-label="Time: activate to sort column ascending"
                          style={{ width: "38px" }}
                        >
                          Time
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {Object.keys(Notifications).length > 0 ? (
                        Notifications.data.map((notification) => (
                          <tr role="row" className="odd">
                            <td className="sorting_1">{notification?.title}</td>
                            <td>
                              {" "}
                              <td>
                                {moment(notification?.createdAt).format(
                                  "Do MMMM YYYY"
                                )}
                              </td>
                            </td>
                            <td>
                              {moment(notification?.createdAt).format(
                                "hh: mm a"
                              )}
                            </td>
                          </tr>
                        ))
                      ) : (
                        <h4>No notification found...</h4>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12 col-md-5">
                  <div
                    className="dataTables_info"
                    id="DataTables_Table_0_info"
                    role="status"
                    aria-live="polite"
                  >
                    Showing 1 to 2 of 2 entries
                  </div>
                </div>
                <div className="col-sm-12 col-md-7">
                  <div
                    className="dataTables_paginate paging_simple_numbers"
                    id="DataTables_Table_0_paginate"
                  >
                    <ul className="pagination">
                      <Pagination
                        perPage={Notifications && Notifications.perPage}
                        total={Notifications && Notifications.total}
                        handlePageClick={handlePageClick}
                      />
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Notifications.propTypes = {
  getUsers: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  // UpdateluggerStatus:PropTypes.func.isRequired,
  getNotifications: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, { getNotifications })(Notifications);
//
