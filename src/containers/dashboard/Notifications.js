import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "../layout/Spinner";
// import ProfileItem from './ProfileItem';
import ReactPaginate from "react-paginate";
import { getNotifications } from "../../actions/profileAction";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link, withRouter } from "react-router-dom";
import moment from "moment";
import { handlenotifcation } from "../../utils/commonFunctions";
import ViewLoader from "../../Components/ViewLoader";

const Notifications = ({
  getNotifications,
  profile: { Users, loading, Notifications },
  history,
}) => {
  // const [notifications, setNotification] = useState({});
  //   const [UsersPerPage] = useState(5);
  const [page, setPage] = useState(1);

  useEffect(() => {
    // getNotifications(page);
  }, [page]);

  const onPage = (selectedPage) => {
    alert(selectedPage);
    setPage(selectedPage.selected + 1);
  };
  const onPageLoad = () => {
    getNotifications(page);
  };
  const handlePageClick = (data) => {
    console.log(data);
    let selected = data.selected;
    setPage(selected);
  };

  return (
    <Fragment>
      {loading ? (
     
        <ViewLoader  fontSize="100px"/>
      ) : (
        <Fragment>
            <div className="app-content content view user all-notifications">
        <div className="content-wrapper">
          <div className="content-body">
            <section id="configuration" className="search view-cause">
              <div className="row">
                <div className="col-12">
                  <div className="add-user">
                    <div className="card rounded pad-20">
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <div className="row">
                            <div className="col-12">
                              <h1 className="pull-left">Notifications</h1>
                            </div>
                          </div>

                          <div className="row">
                          {Object.keys(Notifications).length > 0 ? (
                              Notifications.data.map((notification) => (
                                 <div className="col-12">
                                <div className="noti-inner-cards mb-3">
                                  <div className="card mt-2">
                                    <div className="noti-content">
                                      <div className="media align-items-center">
                                        <i className="fa fa-bell-o align-self-start" aria-hidden="true" />
                                        <div className="media-body">
                                          <h5>{notification?.title}</h5>
                                          <p className="fc-yellow">{moment(notification?.createdAt).format('DD-MM-YYYY | hh:mm:a')}</p>
                                        </div>
                                        <div className="noti-right">
                                          <a onClick={()=>handlenotifcation(notification,history)} >View</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              ))
                            ) : (
                              <h4>No notification found...</h4>
                            )}
                      
                       
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>

        </Fragment>
      )}
    </Fragment>
  );
};

Notifications.propTypes = {
  getUsers: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  // UpdateluggerStatus:PropTypes.func.isRequired,
  getNotifications: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, { getNotifications })(withRouter(Notifications));
//
