import React from 'react'

const Footer = () => {
    return (
        <div>
              <div className="login-fail-main">
  <div className="featured inner">
    <div className="modal fade common-modal qr-code" tabIndex role aria-labelledby aria-hidden="true"> 
      <div className="modal-dialog modal-lgg">
        <div className="modal-content">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <div className="modal-main">
            <div className="modal-inner">
              <form action>
                <div className="row">
                  <div className="col-12 text-center">
                    <img src="images/cnfrm-icon.png" className="img-fluid" alt="" /> 
                    <h3>Are you sure you want to download all QR Codes?</h3>  
                    <p className="text-danger">Note: QR Codes will be exported in the Excel  Sheet along with the orgnization details.</p>
                    <button type="button" className="btn-default fill-btn" data-dismiss="modal" aria-label="Close">Yes</button>
                    <button type="button" className="btn-default outline-btn" id>No</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




        </div>
    )
}

export default Footer
