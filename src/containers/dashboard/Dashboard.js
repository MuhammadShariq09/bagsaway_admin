import React, { useEffect, Fragment, useState } from 'react'
import { Link, withRouter, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getDashboard,getDashboardGraphs } from '../../actions/dashboardRoute'
import { Bar } from "react-chartjs-2";
import { logout } from '../../actions/authAction'
import ActivityLog from './ActivityLog'

// import { VectorMap } from '@south-paw/react-vector-maps';



const Dashboard = ({history, getDashboard,auth:{isAuthenticated} ,logout, dashboard: { Dashboard,loading } }) => {
  let currentyear  = new Date().getFullYear()
  let [year, setyear] = useState(currentyear)
  let [organization, setOrganization] = useState('')
  let [countryCode, setCountryCode] = useState('')

  let [options, setOptions] = useState('')


  useEffect(() => {
   
    getDashboard(year)
  }, [getDashboard,year])


  


  const data = {
    labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
    datasets: [
      {
        label: 'Total Requests per month',
        data:Dashboard&&Dashboard?.total_requests,
        borderColor: [
          'rgba(0, 0, 0, 0.7)',
          'rgba(0, 0, 0, 0.7)'
  
        ],
        backgroundColor: [
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
  
  
        ]
      }
  
    ]
  }
  const data2 = {
    labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
    datasets: [
      {
        label: '',
        data:[],
        borderColor: [
          'rgba(0, 0, 0, 0.7)',
          'rgba(0, 0, 0, 0.7)'
  
        ],
        backgroundColor: [
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
          'rgba(210,160, 11,1)',
  
  
        ]
      }
  
    ]
  }
  
  
  const options1 = {
    title: {
      display: true,
      text: 'Bar Chart'
    },
    scales: {
      yAxes: [
        {
          ticks: {
            min: 1,
            max: 10,
            stepSize: 1
          }
        }
      ]
    }
  }


if(!isAuthenticated){
  return   history.replace('/')

}




  return (
    
    <div className="app-content content view user">
    <div className="content-wrapper">
      <div className="content-body">
        {/* Basic form layout section start */}
        <section id="configuration" className="search view-cause dashboard">
          {/* ROW START */}
          <div className="row">
            <div className="col-xl-6 col-md-6 col-12 ">
              <div className="grayBx">
                <div className="d-flex flex-row justify-content-between">
                  <div className="p-2">
                    <h3 className="fc-blue">TOTAL USERS</h3>
                    <h1 className="fc-blue mt-4">{Dashboard&&Dashboard?.total_counts?.totalstats?.total_users}</h1>
                  </div>
                  <div className="p-2 text-right">
                    <h1 className="fc-blue"><i className="fas fa-user-check" /></h1>
                    {/* <h1 className="fc-blue"><i className="fas fa-long-arrow-alt-up fc-green mt-3" /> 100.0%</h1> */}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-md-6 col-12 ">
              <div className="grayBx">
                <div className="d-flex flex-row justify-content-between">
                  <div className="p-2">
                    <h3 className="fc-blue">TOTAL Requests</h3>
                    <h1 className="fc-blue mt-4">{Dashboard&&Dashboard?.total_counts?.totalstats?.total_deliveries}</h1>
                  </div>
                  <div className="p-2 text-right">
                    <h1 className="fc-blue"><i className="fas fa-user-check" /></h1>
                    {/* <h1 className="fc-blue"><i className="fas fa-long-arrow-alt-up fc-green mt-3" /> 100.0%</h1> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* ROW END */}
          <div className="row">
            <div className="col-12">
              <div className="card rounded pad-20">
                <div className="card-content collapse show">
                  <div className="card-body table-responsive card-dashboard">
                    {/* Basic Column Chart */}
                    <div className="row">
                      <div className="col-xl-12 col-lg-12 col-md-12">
                        <div className="row justify-content-between">
                          <div className="col-md-9 col-12">
                            <h1>Charts</h1>
                          </div>
                          <div className="col-md-3 col-12">
                            <select onChange={(e)=>setyear(e.target.value)}>
                              <option value={new Date().getFullYear()}>Select Year</option>
                              <option value={new Date().getFullYear()}>{new Date().getFullYear()}</option>
                              <option value={new Date().getFullYear()-1}> {new Date().getFullYear()-1}</option>
                              <option value={new Date().getFullYear()-2}   >{new Date().getFullYear()-2}</option>
                            </select>
                          </div>
                        </div>
                        <div className="media home-chart align-items-center mt-2">
                          {/* <h5 className="text-center">Revenue</h5> */}

                          <div className="media-body">

                           <Bar  data={data} options={options1}/>
                            <div id="smooth-area-chart" className="height-400 w-100 mb-2" />
                            {/* <h5 className="text-center month-text">Months</h5> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  )




}

Dashboard.propTypes = {

}

const mapStateToProps = (state) => ({

  auth: state.auth,
  dashboard: state.dashboard,
  organization:state.organization
 
})

export default connect(mapStateToProps, { getDashboard ,logout })(withRouter(Dashboard))
