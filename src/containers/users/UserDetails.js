import React,{useEffect,Fragment,useState} from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import moment from 'moment'
import {Link,withRouter} from 'react-router-dom'

import { getCurrentProfile,getUserById,updateUser } from '../../actions/profileAction';
import { ToastContainer } from 'react-toastify';



 const UserDetails = ({ getUserById, updateUser,profile: { profile,loading }, auth, match,history }) => {
    const [update, setUpdate] = useState(false)


    const [formData,setFormData] = useState({
      
        firstname:'',
        lastname:'',
        email:'',
        phone_no:'',
        createdAt:'',
        image:''
 
    });
    const {firstname,lastname,email,phone_no,createdAt,image} = formData




    useEffect(() => {
      getUserById(match.params.id);
     
      }, [getUserById, match.params.id]);




      useEffect(() => {
      
        if(profile!==null){
          setFormData({
            firstname: loading || !profile.firstname? '' : profile.firstname,
            lastname: loading || !profile.lastname ? '' : profile.lastname,
            email:loading || !profile.email ? '' : profile.email,
            phone_no:loading || !profile.phone_no ? '' : profile.phone_no,
            createdAt:loading || !profile.createdAt ? '' : profile.createdAt,
            image:loading || !profile.image ? '' : profile.image

  
          });
        }
  
     

      }, [loading,profile]);

    
    const onchange=(e)=>{
        console.log(e.target.name)
        console.log(e.target.value)
        setFormData({...formData,[e.target.name]:e.target.value})

    }

  
    const onSubmit= (e)=>{
        e.preventDefault()
        // console.log("firstname,lastname")
        
        updateUser(match.params.id,firstname,lastname,phone_no,history)
     

    }


    
    return (
    

        
            <Fragment>
            
            {profile !== null||profile!=undefined ? (
              <Fragment>
                  <div className="app-content content view user">
        <div className="content-wrapper">
          <div className="content-body">
            {/* Basic form layout section start */}
            <section id="configuration" className="search view-cause">
              <div className="row">
                <div className="col-12">
                  <div className="card pad-20">
                    <div className="card-content collapse show">
                      <div className="card-body table-responsive card-dashboard">
                        <div className="row">
                          <div className="col-lg-12">
                            <a onClick={()=>history.goBack()}><h1><i className="fas fa-arrow-left" /> User Details</h1></a>
                          </div>
                          <div className="col-lg-12 my-3">
                            <div className="media">
                              <div className="myprofile-img mx-4">
                                <img src={image?image:process.env.PUBLIC_URL+"/images/icon-user.png"} className="img-fluid" alt="" />
                              </div>
                              <div className="media-body">
                                <div className="row">
                                  <div className="col-lg-3">
                                    <p className="bold">First Name</p>
                                    <p>{firstname}</p>  
                                  </div>
                                  <div className="col-lg-3">
                                    <p className="bold">Last Name</p>
                                    <p>{lastname}</p>
                                  </div>
                                  <div className="col-lg-3">
                                    <p className="bold">Email Address</p>
                                    <p>{email}</p>
                                  </div>
                                  <div className="col-lg-3 text-right">
                                    {/* <a ><i className="far fa-edit" /> Edit</a> */}
                                  </div>
                                </div>{/* ROW END */}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card pad-20">
                    <div className="card-content collapse show">
                      <div className="card-body table-responsive card-dashboard">
                        <div className="row mt-2">
                          <div className="col-12 d-block d-sm-flex justify-content-between">
                            <div className="left">
                              <h1>Delivery Request Details</h1>
                            </div>
                            <div className="right">
                            </div>
                          </div>                     
                        </div>
                        <div className="row maain-tabble mt-1">
                          <div id="DataTables_Table_0_wrapper" className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div className="row"><div className="col-sm-12 col-md-6"><div className="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" className="form-control form-control-sm"><option value={10}>10</option><option value={25}>25</option><option value={50}>50</option><option value={100}>100</option></select> entries</label></div></div><div className="col-sm-12 col-md-6"><div id="DataTables_Table_0_filter" className="dataTables_filter"><label>Search:<input spellCheck="true" type="search" className="form-control form-control-sm" placeholder="Search" aria-controls="DataTables_Table_0" /></label></div></div></div><div className="row"><div className="col-sm-12"><table className="table table-striped table-bordered zero-configuration dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                  <thead>
                                    <tr role="row"><th className="sorting_asc" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-sort="ascending" aria-label="S.No: activate to sort column descending" style={{width: '87.6406px'}}>S.No</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="ID Number: activate to sort column ascending" style={{width: '154.328px'}}>ID Number</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="Status: activate to sort column ascending" style={{width: '132.547px'}}>Status</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="Request Date: activate to sort column ascending" style={{width: '193.891px'}}>Request Date</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="Total Cost: activate to sort column ascending" style={{width: '170.172px'}}>Total Cost</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="Fed-Ex Cost: activate to sort column ascending" style={{width: '180.578px'}}>Fed-Ex Cost</th><th className="sorting" tabIndex={0} aria-controls="DataTables_Table_0" rowSpan={1} colSpan={1} aria-label="Action: activate to sort column ascending" style={{width: '120.469px'}}>Action</th></tr>
                                  </thead>
                                  <tbody>
                                    <tr role="row" className="odd">
                                      <td className="sorting_1">01</td>
                                      <td>Bag002</td>
                                      <td className="fc-yellow">Pending</td>
                                      <td>mm/dd/yyyy</td>
                                      <td>$3000</td>
                                      <td>$100</td>
                                      <td>
                                        <div className="btn-group mr-1 mb-1">
                                          <button type="button" className="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="fa fa-ellipsis-v" /></button>
                                          <div className="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(4px, 23px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                            <a className="dropdown-item" href="user-profile.php"><i className="fa fa-eye" />View</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr><tr role="row" className="even">
                                      <td className="sorting_1">01</td>
                                      <td>Bag002</td>
                                      <td className="fc-green">Approved</td>
                                      <td>mm/dd/yyyy</td>
                                      <td>$3000</td>
                                      <td>$100</td>
                                      <td>
                                        <div className="btn-group mr-1 mb-1">
                                          <button type="button" className="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="fa fa-ellipsis-v" /></button>
                                          <div className="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(4px, 23px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                            <a className="dropdown-item" href="user-profile.php"><i className="fa fa-eye" />View</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr><tr role="row" className="odd">
                                      <td className="sorting_1">01</td>
                                      <td>Bag002</td>
                                      <td className="fc-red">Rejected</td>
                                      <td>mm/dd/yyyy</td>
                                      <td>$3000</td>
                                      <td>$100</td>
                                      <td>
                                        <div className="btn-group mr-1 mb-1">
                                          <button type="button" className="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="fa fa-ellipsis-v" /></button>
                                          <div className="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(4px, 23px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                            <a className="dropdown-item" href="user-profile.php"><i className="fa fa-eye" />View</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr></tbody>
                                </table></div></div><div className="row"><div className="col-sm-12 col-md-5"><div className="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div></div><div className="col-sm-12 col-md-7"><div className="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><ul className="pagination"><li className="paginate_button page-item previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx={0} tabIndex={0} className="page-link">Previous</a></li><li className="paginate_button page-item active"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx={1} tabIndex={0} className="page-link">1</a></li><li className="paginate_button page-item next disabled" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx={2} tabIndex={0} className="page-link">Next</a></li></ul></div></div></div></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
                     
            </Fragment>
            
        
    
        

    ):'<h1>user Details doesonot found</h1>'

}
</Fragment>

)
}

UserDetails.propTypes = {
    updateProfile:PropTypes.func.isRequired,
    getCurrentProfile: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
  
  };


const mapStateToProps = (state) => ({
    auth: state.auth,
    profile:state.profile

  });
  
  export default connect(mapStateToProps, {updateUser,getUserById,getCurrentProfile })(withRouter(UserDetails));


               
