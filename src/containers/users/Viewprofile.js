import React, { useEffect, Fragment, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { withRouter } from "react-router-dom";
import { getCurrentProfile, updateProfile } from "../../actions/profileAction";
import { updatePassword } from "../../actions/authAction";
import { useForm } from "react-hook-form";

toast.configure();
const ViewProfile = ({
  getCurrentProfile,
  updateProfile,
  updatePassword,
  auth,
  profile: { currentProfile, loading },
  history,
}) => {
  const { handleSubmit, register, errors, getValues } = useForm();
  const [update, setUpdate] = useState(false);

  const [formData, setFormData] = useState({
    firstname: "",
    lastname: "",
    phone_no: "",
    currentpassword: "",
    newpassword: "",
    confirmpassword: "",
    image: "",
  });
  const [picture, setPicture] = useState(null);
  const [imgData, setImgData] = useState(null);
  const {
    firstname,
    lastname,
    email,
    phone_no,
    currentpassword,
    confirmpassword,
    image,
    newpassword,
  } = formData;

  useEffect(() => {
    getCurrentProfile();
  }, [loading, getCurrentProfile]);

  useEffect(() => {
    if (currentProfile && currentProfile.user != null) {
      setFormData({
        firstname:
          loading || !currentProfile.user.firstname
            ? ""
            : currentProfile.user.firstname,
        lastname:
          loading || !currentProfile.user.lastname
            ? ""
            : currentProfile.user.lastname,
        phone_no:
          loading || !currentProfile.user.phone_no
            ? ""
            : currentProfile.user.phone_no,
        email:
          loading || !currentProfile.user.lastname
            ? ""
            : currentProfile.user.email,
        image:
          loading || !currentProfile.user.image
            ? ""
            : currentProfile.user.image,
      });
    }
  }, [currentProfile]);

  const onchange = (e) => {
    console.log(e.target.name);
    console.log(e.target.value);
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit =  (e) => {
    e.preventDefault();
    console.log("firstname,lastname");
    updateProfile(firstname, lastname, phone_no, image, history);
    setUpdate(false);
  };
  const handlePasswordSubmit = async (data, e) => {
    e.preventDefault();
    console.log(currentpassword, newpassword, confirmpassword);

    updatePassword(currentpassword, newpassword, confirmpassword, history);
    setFormData({
      currentpassword: "",
      newpassword: "",
      confirmpassword: "",
    });
    // e.target.reset()
  };

  const onChangePicture = (e) => {
    e.preventDefault();

    if (e.target.files[0]) {
      //   alert(e.target.files[0])
      // console.log("picture: ", e.target.files);
      setPicture(e.target.files[0]);
      // alert(picture)
      const reader = new FileReader();
      reader.onload = () => {
        setImgData(reader.result);
        setFormData({ ...formData, image: reader.result });
      };
      // alert(imgData)
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <Fragment>
      {currentProfile !== null ? (
        <Fragment>
          <div>
            <div className="app-content content view user profile">
              <div className="content-wrapper">
                <div className="content-body">
                  {/* Basic form layout section start */}
                  <section id="configuration" className="search view-cause">
                    <div className="row">
                      <div className="col-12">
                        <div className="card pad-20">
                          <div className="card-content collapse show">
                            <div className="card-body table-responsive card-dashboard">
                            <form onSubmit={(e) => onSubmit(e)}>

                              <div className="row">
                                <div className="col-12 d-block d-sm-flex justify-content-between">
                                  <div className="left">
                                    <h1 className="pull-left">Profile</h1>
                                  </div>
                                  <div className="right"></div>
                                </div>
                              </div>
                                <div className="row mt-4">
                                  <div className="col-lg-12">
                                    <div className="media">
                                      <div className="myprofile-img mx-4 position-relative">
                                        <img
                                          src={
                                            image
                                              ? image
                                              : process.env.PUBLIC_URL +
                                                "images/student-pro-girl.png"
                                          }
                                          className="img-fluid"
                                          alt=""
                                        />
                                        <input type="file" id="upload" className="hidden" onChange={(e)=>onChangePicture(e)} />

                                        {update ? (
                                          <button
                                            name="file"
                                            className="camera-btn"
                                            type="button"
                                            onClick={() =>
                                              document
                                                .getElementById("upload")
                                                .click()
                                            }
                                          >
                                            <i className="fa fa-camera"></i>
                                          </button>
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                      <div className="media-body">
                                        <div className="row">
                                          <div className="col-lg-4">
                                            <p className="bold">First Name</p>
                                            <input
                                              type="text"
                                              value={firstname}
                                              name="firstname"
                                              onChange={(e) => onchange(e)}
                                              className="form-control"
                                              placeholder="enter firstname"
                                              disabled={update?false:true}
                                           
                                            />
                                          </div>
                                          <div className="col-lg-4">
                                            <p className="bold">Last Name</p>
                                            <input
                                              type="text"
                                              value={lastname}
                                              name="lastname"
                                              onChange={(e) => onchange(e)}
                                              className="form-control"
                                              placeholder="enter lastname"
                                              disabled={update?false:true}

                                          
                                            />
                                          </div>
                                          <div className="col-lg-4">
                                            <p className="bold">
                                              Email Address
                                            </p>
                                            <p>{email}</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="row mt-1 mb-3">
                                  <div className="col-12">
                                    {/* <a
                                    className="btn-dblue bs-1"
                                  >
                                    UPDATE
                                  </a> */}
                                    {!update ? (
                                      <a
                                      
                                        onClick={() => setUpdate(!update)}
                                        className="btn-dblue "
                                      >
                                        Edit
                                      </a>
                                    ) : (
                                      <button
                                        type="submit"
                                        className="btn-dblue bs-1"
                                      >
                                        Save
                                      </button>
                                    )}
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
           </div>

          <ToastContainer autoClose={3000} />
        </Fragment>
      ) : (
        "<h1>user Details doesonot found</h1>"
      )}
    </Fragment>
  );
};

ViewProfile.propTypes = {
  updateProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  updatePassword: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
});

export default connect(mapStateToProps, {
  updateProfile,
  getCurrentProfile,
  updatePassword,
})(withRouter(ViewProfile));
