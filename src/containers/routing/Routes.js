import React, { lazy, Suspense } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";



const Dashboard = lazy(() => import("../dashboard/Dashboard"));
const Users = lazy(() => import("../users/Users"));
const NotFound = lazy(() => import("../layout/NotFound"));
const PrivateRoute = lazy(() => import("../routing/PrivateRoute"));
const Viewprofile = lazy(() => import("../users/Viewprofile"));
const UserDetails = lazy(() => import("../users/UserDetails"));
const Feedback = lazy(() => import("../Feedback/Feedback"));
const FeedbackDetails = lazy(() => import("../Feedback/FeedbackDetails"));
const Deliveries = lazy(() => import("../deleiveries/deliveries"));
const DeliveriesDetail = lazy(() => import("../deleiveries/deliveryDetail"));
const Reports = lazy(() => import("../reports/reports"));
const ReportDetail = lazy(() => import("../reports/reportDetail"));
const Notifications = lazy(() => import("../dashboard/Notifications"));
const Chat = lazy(() => import("../chat/chat"));


const Routes = (props) => {
  return (/*  */
    <section>
      <Switch>
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute exact path="/userdetail" component={UserDetails} />
        <PrivateRoute exact path="/profile" component={Viewprofile} />
        <PrivateRoute exact path="/feedback" component={Feedback} />
        <PrivateRoute exact path="/deliveries" component={Deliveries} />
        <PrivateRoute exact path="/delivery-detail/:id" component={DeliveriesDetail} />
        <PrivateRoute  exact path="/feedbackdetails/:id" component={FeedbackDetails} />
        <PrivateRoute exact path="/reports" component={Reports} />
        <PrivateRoute exact path="/report-detail/:id" component={ReportDetail} />

        <PrivateRoute exact path="/notifications" component={Notifications}/> 
        <PrivateRoute exact path="/users" component={Users} />
        <PrivateRoute exact path="/chat" component={Chat} />

      
        <PrivateRoute exact path="/viewuser/:id" component={UserDetails} />

        <Route component={NotFound} />
      </Switch>
    </section>
  );
};

export default Routes;
