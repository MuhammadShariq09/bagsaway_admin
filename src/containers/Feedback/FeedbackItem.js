import React, { Fragment, useState } from 'react'
import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { UpdateUserStatus } from '../../actions/profileAction';
import Spinner from '../layout/Spinner';
import { ToastContainer } from "react-toastify";
import {getSerial} from '../../utils/commonFunctions'
import moment from 'moment'

 const FeedbackItem = ({data,index,perPage,currentPage, handleDelete,selection}) => {
    return (
    <Fragment>
      {data === null ? (
        <Spinner />
      ) : (
        <Fragment>
          
     
        <tr role="row" className="odd">
                                      <td className="sorting_1">{getSerial(perPage,currentPage,index)}</td>
                                      <td>{data &&data?.firstname}</td> 
                                      <td>{data &&data?.lastname}</td> 
                                      <td>{data &&data?.email}</td> 
                                       <td>{moment(data && data?.createdAt).format('Do-MMMM-YYYY')}</td>
                                      <td>
                                        <div className="btn-group mr-1 mb-1">
                                          <button type="button" className="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="fa fa-ellipsis-v" /></button>
                                          <div className="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(4px, 23px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                          <Link className="dropdown-item uppercase" to={`/feedbackdetails/${data._id}`}>
                                              <i className="fa fa-eye" />
                                              View
                                            </Link>
                                            <a className="dropdown-item" href="#" data-toggle="modal" data-target="#delete">
                                              <i className="fa fa-trash" />
                                              Delete
                                            </a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
      
        
        



      
      </Fragment>
    )
      }
    </Fragment>
    )
}

FeedbackItem.propTypes = {
 
  // UpdateUserStatus:PropTypes.func.isRequired
  


};


export default connect(null, {})(FeedbackItem);

