import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Spinner from "../layout/Spinner";
import ReactPaginate from "react-paginate";
import {
  getFeedbacks,
  SortAction,
  deleteFeedback,
} from "../../actions/feedbackAction";
import { Link } from "react-router-dom";
import FeedbackItem from "./FeedbackItem";
import { ToastContainer } from "react-toastify";
import Pagination from "../../utils/pagination";
// import { deleteReport } from '../../actions/reports';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ViewLoader from "../../Components/ViewLoader";

const Feedback = ({
  getFeedbacks,
  deleteFeedback,
  SortAction,
  feedbacks: { loading, Feedbacks },
  loader : {Loader}
}) => {
  const [currentID, setCurrentID] = useState("");
  const [OrderState, setOrderState] = useState(1);
  const [page, setPage] = useState(1);
  const [limit, setlimit] = useState(5);
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [keyword, setKeyword] = useState("");

  const handleKeyword = (e) => {
    let value = e.target.value;

    setKeyword(value);
  };

  useEffect(() => {
    // alert(Selection)
    getFeedbacks(page, limit, from, to, keyword);
  }, [getFeedbacks, page, limit, from, to, keyword]);

  const handleDelete = (Id) => {
    // alert("Id,ID",Id)
    setCurrentID(Id);
  };

  const toggleOrder = () => {
    const currentState = OrderState === 1 ? -1 : 1;
    setOrderState(currentState);
    return OrderState;
  };
  //   const handleSelection=()=>{
  //     const currentState = Selection==1?0:1
  //       setSelection(currentState)
  //       return Selection
  //   }
  const handleChange = (e) => {
    // alert(e.target.value)
    let value = e.target.value;

    setlimit(value);
  };
  const handlePageClick = (data) => {
    let selected = data.selected + 1;
    // console.log(selected)
    setPage(selected);
  };
  // const paginate = pageNumber => setCurrentPage(pageNumber);
  return (
    <Fragment>
      {loading ? (
        <ViewLoader fontSize="100px" />
      ) : (
        <Fragment>

          
          <div className="app-content content view user">
            <div className="content-wrapper">
              <div className="content-body">
                {/* Basic form layout section start */}
                <section id="configuration" className="search view-cause">
                  <div className="row">
                    <div className="col-12">
                      <div className="card pad-20">
                        <div className="card-content collapse show">
                          <div className="card-body table-responsive card-dashboard">
                            <div className="row mt-2">
                              <div className="col-12 d-block d-sm-flex justify-content-between">
                                <div className="left">
                                  <h1>Feedbacks</h1>
                                </div>
                                <div className="right text-right"></div>
                              </div>
                            </div>
                            <div className="row maain-tabble mt-1">
                              <div
                                id="DataTables_Table_0_wrapper"
                                className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
                              >
                                <div className="row">
                                  <div className="col-sm-12 col-md-6">
                                    <div
                                      className="dataTables_length"
                                      id="DataTables_Table_0_length"
                                    >
                                      <label>
                                        Show{" "}
                                        <select
                                        onChange={(e)=>handleChange(e)}
                                          name="DataTables_Table_0_length"
                                          aria-controls="DataTables_Table_0"
                                          className="form-control form-control-sm"
                                        >
                                          <option value={10}>10</option>
                                          <option value={25}>25</option>
                                          <option value={50}>50</option>
                                          <option value={100}>100</option>
                                        </select>{" "}
                                        entries
                                      </label>
                                    </div>
                                  </div>
                                  <div className="col-sm-12 col-md-6">
                                    <div
                                      id="DataTables_Table_0_filter"
                                      className="dataTables_filter"
                                    >
                                      <label>
                                        Search:
                                        <input
                                            onChange={(e) => handleKeyword(e)}
                                          spellCheck="true"
                                          type="search"
                                          className="form-control form-control-sm"
                                          placeholder="Search"
                                          aria-controls="DataTables_Table_0"
                                        />
                                      </label>
                                    </div>
                                  </div>
                                </div>
{
  Loader ? <ViewLoader fontSize="70px" /> : 

<>
                                <div className="row">
                                  <div className="col-sm-12">
                                    <table
                                      className="table table-striped table-bordered zero-configuration dataTable no-footer"
                                      id="DataTables_Table_0"
                                      role="grid"
                                      aria-describedby="DataTables_Table_0_info"
                                    >
                                      <thead>
                                        <tr role="row">
                                          <th
                                            className="sorting_asc"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-sort="ascending"
                                            aria-label="S.No: activate to sort column descending"
                                            style={{ width: "108.812px" }}
                                          >
                                            S.No
                                          </th>
                                          <th
                                            className="sorting"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-label="First Name: activate to sort column ascending"
                                            style={{ width: "201.812px" }}
                                          >
                                            First Name
                                          </th>
                                          <th
                                            className="sorting"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-label="Last Name: activate to sort column ascending"
                                            style={{ width: "194.25px" }}
                                          >
                                            Last Name
                                          </th>
                                          <th
                                            className="sorting"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-label="Email Address: activate to sort column ascending"
                                            style={{ width: "254.547px" }}
                                          >
                                            Email Address
                                          </th>
                                          <th
                                            className="sorting"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-label="Date: activate to sort column ascending"
                                            style={{ width: "188.734px" }}
                                          >
                                            Date
                                          </th>
                                          <th
                                            className="sorting"
                                            tabIndex={0}
                                            aria-controls="DataTables_Table_0"
                                            rowSpan={1}
                                            colSpan={1}
                                            aria-label="Action: activate to sort column ascending"
                                            style={{ width: "149.469px" }}
                                          >
                                            Action
                                          </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {Feedbacks.data &&
                                        Object.keys(Feedbacks).length > 0 ? (
                                          Feedbacks.data.map((item, index) => (
                                            <FeedbackItem
                                              key={item._id}
                                              data={item}
                                              handleDelete={handleDelete}
                                              perPage={Feedbacks.perPage}
                                              currentPage={
                                                Feedbacks.currentPage
                                              }
                                              index={index}
                                            />
                                          ))
                                        ) : (
                                          <tr class="odd">
                                          <td
                                            valign="top"
                                            colspan="8"
                                            class="dataTables_empty"
                                          >
                                            No matching records found
                                          </td>
                                        </tr>
                                        )}
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-sm-12 col-md-5">
                                    <div
                                      className="dataTables_info"
                                      id="DataTables_Table_0_info"
                                      role="status"
                                      aria-live="polite"
                                    >
                                      Showing 1 to 3 of 3 entries
                                    </div>
                                  </div>
                                  <div className="col-sm-12 col-md-7">
                                    <div
                                      className="dataTables_paginate paging_simple_numbers"
                                      id="DataTables_Table_0_paginate"
                                    >
                                    
                                      <ul className="pagination">
                                        <Pagination
                                          perPage={
                                            Feedbacks && Feedbacks.perPage
                                          }
                                          total={Feedbacks && Feedbacks.total}
                                          handlePageClick={handlePageClick}
                                        />
                                      </ul>
                                    
                                    </div>
                                  </div>
                                </div>
</>
}
                         </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
 
        </Fragment>
      )}
    </Fragment>
  );
};

Feedback.propTypes = {
  getFeedbacks: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  feedbacks: state.feedback,
  loader : state.loader
});

export default connect(mapStateToProps, {
  getFeedbacks,
  SortAction,
  deleteFeedback,
})(Feedback);
