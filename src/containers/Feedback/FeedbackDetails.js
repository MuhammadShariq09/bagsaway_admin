import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Spinner from "../layout/Spinner";
import { getFeedbackById } from "../../actions/feedbackAction";

const FeedbackDetails = ({
  getFeedbackById,
  feedbacks: { loading, Feedback },
  match,
  history,
}) => {
  console.log("userID", match.params.id);
  useEffect(() => {
    //  console.log(match.params.id)

    getFeedbackById(match.params.id);
  }, [getFeedbackById, match.params.id]);

  return (
    <Fragment>
      {Feedback === null ? (
        <Spinner />
      ) : (
        <Fragment>
          <div className="app-content content view user">
            <div className="content-wrapper">
              <div className="content-body">
                {/* Basic form layout section start */}
                <section id="configuration" className="search view-cause">
                  <div className="row">
                    <div className="col-12">
                      <div className="card pad-20">
                        <div className="card-content collapse show">
                          <div className="card-body table-responsive card-dashboard">
                            <div className="row mt-2">
                              <div className="col-12 d-block d-sm-flex justify-content-between">
                                <div className="left">
                                  <a onClick={() => history.goBack()}>
                                    <h1>
                                      <i className="fas fa-arrow-left" />{" "}
                                      Feedback Details
                                    </h1>
                                  </a>
                                </div>
                                <div className="right text-right"></div>
                              </div>
                            </div>
                            <div className="row my-1">
                              <div className="col-lg-3 my-1">
                                <p>
                                  <strong>First Name</strong>
                                </p>
                                <p>{Feedback.firstname}</p>
                              </div>
                              <div className="col-lg-3 my-1">
                                <p>
                                  <strong>Last Name</strong>
                                </p>
                                <p>{Feedback.lastname}</p>
                              </div>
                              <div className="col-lg-3 my-1">
                                <p>
                                  <strong>Email Address</strong>
                                </p>
                                <p>{Feedback.email}</p>
                              </div>
                              <div className="col-lg-3 my-1">
                                <p>
                                  <strong>Subject</strong>
                                </p>
                                <p>{Feedback.subject}</p>
                              </div>
                              <div className="col-lg-12 my-1">
                                <p>
                                  <strong>Message </strong>
                                </p>
                                <p>{Feedback.message}</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

FeedbackDetails.propTypes = {
  getFeedbackById: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  feedbacks: state.feedback,
});

export default connect(mapStateToProps, { getFeedbackById })(FeedbackDetails);
