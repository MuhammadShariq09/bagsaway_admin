import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import {
  login,
  forgotPassword,
  resetPassword,
  verifyCode,
} from "../../actions/authAction";

const Login = ({
  isAuthenticated,
  isAdmin,
  login,
  forgotPassword,
  resetPassword,
  code,
  verifyCode,
  history,
}) => {
  // const [refresh, setstate] = useState(false)
  const [passtype, Setpasstype] = useState(false);
  const [passtype1, Setpasstype1] = useState(false);
  const [passtype2, Setpasstype2] = useState(false);

  const [formData, setFormData] = useState({
    email: "",
    password: "",
    forgotemail: "",
    resetCode: "",
    newPassword: "",
    confirmPassword: "",
  });

  const {
    email,
    password,
    forgotemail,
    resetCode,
    newPassword,
    confirmPassword,
  } = formData;
  const onchange = (e) => {
    console.log(e.target.name);
    console.log(e.target.value);
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    console.log("email,password");
    // console.log(email,password)
    login(email, password, true);
  };

  const handleForgot = async (e) => {
    e.preventDefault();
    console.log(forgotemail);
    // alert("called")
    forgotPassword(forgotemail, history);
  };

  const handleVerfyCode = async (e) => {
    e.preventDefault();
    // console.log(resetCode)
    verifyCode(resetCode, history);
  };

  const handleReset = async (e) => {
    e.preventDefault();
    console.log(newPassword, confirmPassword, code);
    resetPassword(newPassword, confirmPassword, code, "admin", history);
  };

  //Redirect if logged int
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <>
      <div>
        <section className="login-main">
          <div className="container">
            <div className="login-inner">
              <div className="row align-items-center m-0">
                <div className="col-lg-6 col-12 p-0">
                  <div className="left">
                    {" "}
                    <img src="images/login.png" className="img-fluid" alt="" />
                  </div>
                </div>
                <div className="col-lg-6 col-12 p-0">
                  <div className="text-center mt-3">
                    <img src="images/logo.png" className="img-fluid" />
                  </div>
                  <div className="right">
                    <h1>LOGIN</h1>
                    <form onSubmit={(e) => onSubmit(e)}>
                      <div className="row">
                        <div className="col-12 form-group mt-3 mb-0">
                          <label>
                            Email Adress <span className="fc-red">*</span>
                          </label>
                          <input
                            type="email"
                            className="form-control bs-1"
                            placeholder="Enter Email"
                            value={email}
                            name="email"
                            onChange={(e) => onchange(e)}
                            required
                          />
                        </div>
                        <div className="col-12 form-group position-relative mb-0">
                          <label>
                            Password <span className="fc-red">*</span>
                          </label>
                          <input
                            type={passtype == false ? "password" : "text"}
                            className="form-control bs-1"
                            placeholder="Password"
                            value={password}
                            name="password"
                            placeholder="Enter Password"
                            onChange={(e) => onchange(e)}
                            required
                          />
                          {/* <i class="fa fa-lock"></i> */}
                          <button
                            type="button"
                            className="view-btn position-absolute"
                          >
                            <i
                              onClick={() => Setpasstype(!passtype)}
                              className={`fa fa-eye${
                                passtype == false ? "-slash" : ""
                              } enter-icon right-icon`}
                              aria-hidden="true"
                            />
                          </button>
                        </div>
                      </div>
                      <div className="d-flex justify-content-between">
                        <div className>
                          <label className="login-check">
                            Remember Me
                            <input type="checkbox" />
                            <span className="checkmark" />
                          </label>
                        </div>
                        <div className>
                          <a
                            href="password-recovery-1.php"
                            className="fc-black forgot-pass"
                            data-toggle="modal"
                            data-target="#myModal"
                          >
                            {" "}
                            Forgot Password?
                          </a>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 text-center mt-3">
                          <button type="submit" className="btn-dblue bs-1">
                            Login
                          </button>
                          <Link to="/" className="login back">
                            {/* 										<i class="fa fa-arrow-circle-left"></i> */}
                            <i className="fas fa-arrow-left" />
                            back to Website
                          </Link>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* MODAL FORGOT PASSWORD START  */}
        <div className="modal fade" id="myModal" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center my-4 px-3">
                <form onSubmit={(e) => handleForgot(e)}>
                  <h2 className="bold">Forgot Password</h2>
                  <div className="row">
                    <div className="col-12 form-group mt-2 mb-0 text-left">
                      <label>Email Address</label>
                      <input
                        type="email"
                        className="form-control bs-1"
                        placeholder="Enter Email"
                        value={forgotemail}
                        name="forgotemail"
                        placeholder="Enter Email Address"
                        onChange={(e) => onchange(e)}
                        required
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 text-center">
                      <button
                        type="submit"
                        className="btn-dblue bs-1"
                        // data-toggle="modal"
                        // data-target="#myModal1"
                        // data-dismiss="modal"
                      >
                        Continue
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* MODAL FORGOT PASSWORD END  */}
        {/* MODAL VERIFICATION CODE START  */}
        <div className="modal fade" id="myModal1" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center my-2 px-3">
                <h2 className="bold">Forgot Password</h2>
                <form onSubmit={(e)=>{handleVerfyCode(e)}}>
                <div className="row">
                  <div className="col-12 form-group mt-2 mb-0 text-left">
                    <label>Verification Code</label>
                    <input
                      type="text"
                      className="form-control bs-1"
                      placeholder="Enter Verification Code"
                      value={resetCode} name="resetCode"  onChange={(e)=>onchange(e)} required
                    />
                  </div>
                  <div className="col-12 text-right mb-2">
                    <a href className="fc-black forgot-pass">
                      Resend Verification Code
                    </a>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <button
                      type="submit"
                      className="btn-dblue bs-1"
                      // data-toggle="modal"
                      // data-target="#myModal2"
                      // data-dismiss="modal"
                    >
                      Continue
                    </button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* MODAL VERIFICATION CODE END  */}
        {/* MODAL NEW PASSWORD START  */}
        <div className="modal fade" id="myModal2" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center p-30-50">
                <h2 className="bold">Forgot Password</h2>
                <form onSubmit={(e)=>handleReset(e)} >
                <div className="row mt-2">
                  <div className="col-12 form-group position-relative mb-0 text-left">
                    <label>New Password</label>
                    <input
                      type="password"
                      className="form-control bs-1"
                      placeholder="Enter New Password"
                      value={newPassword} name="newPassword"  onChange={(e)=>onchange(e)} placeholder="Enter Password" required
                    />
                    {/*          					<button class="view-btn position-absolute"><i class="fa fa-eye"></i></button> */}
                  </div>
                  <div className="col-12 form-group position-relative mb-0 text-left">
                    <label>Re-Type Password *</label>
                    <input
                      type="password"
                      className="form-control bs-1"
                      placeholder="ReType Confirm Password"
                      value={confirmPassword} name="confirmPassword"  onChange={(e)=>onchange(e)}
                      required
                    
                    />
                    {/*          					<button class="view-btn position-absolute"><i class="fa fa-eye"></i></button> */}
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <button
                      type="submit"
                      className="btn-dblue bs-1"
                      // data-toggle="modal"
                      // data-target="#myModalsuccess"
                      // data-dismiss="modal"
                    >
                      UPDATE
                    </button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {/* MODAL NEW PASSWORD END  */}
        {/* MODAL SUCCESS START  */}
        <div className="modal fade" id="myModalsuccess" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center p-30-50">
                <img src="images/icon-success.png" className="img-fluid" />
                <h2 className="bold">Confirmation</h2>
                <p>Your password has been updated!</p>
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <Link to="/login">
                      <button type="button" className="btn-dblue bs-1">
                        OK
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
};
const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, {
  login,
  forgotPassword,
  resetPassword,
  verifyCode,
})(Login);
{
  //   <>
  //   <section className="login-main">
  //     <div className="container">
  //       <div className="login-inner">
  //         <div className="row">
  //           <div className="col-lg-12 col-12 ">
  //             <div className="right">
  //               <div className="logo text-center">
  //                 <img src="images/logo.png" alt="" />
  //               </div>
  //               <h1 className>Sign In</h1>
  //               <form onSubmit={(e) => onSubmit(e)}>
  //                 <div className="row">
  //                   <div className="col-12 form-group position-relative">
  //                     <i className="fa fa-envelope icon" />
  //                     <input
  //                       type="email"
  //                       className="form-control"
  //                       placeholder="Enter Email Address"
  //                       value={email}
  //                       name="email"
  //                       placeholder="Enter Email Address"
  //                       onChange={(e) => onchange(e)}
  //                       required
  //                     />
  //                   </div>
  //                   <div className="col-12 form-group position-relative">
  //                     <i className="fa fa-lock  icon" />
  //                     <input
  //                       type={passtype == false ? "password" : "text"}
  //                       className="form-control enter-input"
  //                       placeholder="Enter Password"
  //                       value={password}
  //                       name="password"
  //                       placeholder="Enter Password"
  //                       onChange={(e) => onchange(e)}
  //                       required
  //                     />
  //                     <button
  //                       className="view-btn position-absolute"
  //                       type="button"
  //                     >
  //                       {" "}
  //                       <i
  //                         onClick={() => Setpasstype(!passtype)}
  //                         className={`fa fa-eye${
  //                           passtype == false ? "-slash" : ""
  //                         } enter-icon right-icon`}
  //                         aria-hidden="true"
  //                       />
  //                     </button>
  //                   </div>
  //                 </div>
  //                 <div className="flex-row">
  //                   <div className="d-flex justify-content-end">
  //                     {/*div class="">
  //                                     <label class="login-check">Remember Me
  //                                         <input type="checkbox" >
  //                                         <span class="checkmark"></span></label>
  //                                 </div*/}
  //                     <div >
  //                       <Link
  //                         to="/forgotpassword"
  //                         className="forgot"
  //                         // data-toggle="modal"
  //                         // data-target="#exampleModalCenter"
  //                       >
  //                         Forgot Password?
  //                       </Link>{" "}
  //                     </div>
  //                   </div>
  //                 </div>
  //                 <div className="row">
  //                   <div className="d-block col-12 text-center">
  //                     <button
  //                       type="submit"
  //                       className="btn-default login fill-btn"
  //                     >
  //                       Sign In
  //                     </button>
  //                   </div>
  //                 </div>
  //               </form>
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   </section>
  //   );
  // </>
}
