import React ,{useState}from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {Link,Redirect} from 'react-router-dom'
import {verifyCode,forgotPassword} from '../../actions/authAction'
import { toast,ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

toast.configure()
const ForgotCode = ({verifyCode,forgotPassword,auth:{isAuthenticated,recoveryEmail},history },) => {

  const [formData,setFormData] = useState({
      
    resetCode:"",
    

});

const {resetCode} = formData
const onchange=(e)=>{
    // console.log(e.target.name)
    // console.log(e.target.value)
    setFormData({...formData,[e.target.name]:e.target.value})

}


const onSubmit= async(e)=>{
    e.preventDefault()
    console.log(resetCode)
    verifyCode(resetCode,history)
 

}





  if(isAuthenticated){
    return <Redirect to="/dashboard"/>

  }
  

    return (
        <div>
          <section className="login-main">
        <div className="container">
          <div className="login-inner">
            <div className="row">
              <div className="col-lg-12 col-12 ">
                <div className="right">
                  <div className="logo text-center">
                    <img src="images/logo.png" alt="" />
                  </div>
                  <h1 className>Password Recovery</h1>
                  <form  onSubmit={e=>onSubmit(e)}> 
                    <div className="row">
                      <div className="col-12 form-group position-relative">
                        <i className="fa fa-pencil-alt icon" />
                        <input
                          type="number"
                          className="form-control"
                          placeholder="Enter Verification Code" name="resetCode" onChange={e=>onchange(e)} required
                        />
                      </div>
                    </div>
                    <div className="flex-row">
                      <div className="d-flex justify-content-end">
                      
                        <div className>
                          {" "}
                          <a
                            href="#"
                            className="forgot"
                            data-toggle="modal"
                            data-target="#exampleModalCenter"
                            onClick={()=>  forgotPassword(recoveryEmail, history)}
                          >
                            Resend Code
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="d-block col-12 text-center btn-bar">
                        <button
                          type="submit"
                          className="btn-default outline-btn"
                        >
                          Continue
                        </button>
                        <Link to="/" className="btn-default fill-btn mt-1">
                          Back To Sign In
                        </Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
          
        </div>
    )
}
ForgotCode.propTypes = {

  verifyCode: PropTypes.func.isRequired,
  
};
const mapStateToProps = (state) => ({
  passwordRecovery:state.passwordRecovery,
 auth : state.auth,


 

});

export default connect(mapStateToProps, {verifyCode,forgotPassword })(ForgotCode);

{
//   <div className="container-fluid admin-login pl-0">
//   <div className="row">
//     <div className="col-5 pr-0">
//       <div className="admin-login-inner d-flex align-items-center justify-content-center">
//         <img src={process.env.PUBLIC_URL+"/images/logo-bg.png"} alt="" className="img-fluid" />
//       </div>
//     </div>
//     <div className="col-xl-4 col-lg-5 py-3 py-lg-0 col-md-6 my-auto ml-3">
//       <div className>
//         <div className="admin-login-card w-100 p-5">
//           <div className="text-left">
//             <h4 className="medium clr-orange">Password Recovery</h4>
//       <form  onSubmit={e=>onSubmit(e)}> 
//             <hr className="blue_line bck-orange" />
//             <div className="md-form md-outline mb-0 input-with-pre-icon">
//               <i className="fas fa-pencil-alt input-prefix active" />
//               <input type="text" id="prefixInside2" className="form-control py-2" placeholder="Enter Verification Code" name="resetCode" onChange={e=>onchange(e)} />
//             </div>
//             <p className="l-grey text-center mt-3">Did not receive email? <a href className="clr-orange">
//                 Send Again</a> 30 sec</p>
//             <a href="password-recovery-3.html"><button className="w-100 mt-4">Continue</button></a>
//             <div className="mt-3 text-center">
//               <Link to="/login" className="medium clr_grey"><i className="fa fa-arrow-circle-left mr-2" /> Back
//                 To Website</Link>
//             </div>
//             </form> 
//           </div>
//         </div>
//       </div>
//     </div>
//   </div>
// </div>
// <ToastContainer autoClose={2000} />

}