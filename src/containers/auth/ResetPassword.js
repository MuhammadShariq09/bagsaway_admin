import React, { useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { Link, Redirect } from "react-router-dom";
import { resetPassword } from "../../actions/authAction";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ResetPassword = ({ resetPassword, isAuthenticated, code, history }) => {
  const [formData, setFormData] = useState({
    newPassword: "",
    confirmPassword: "",
  });

  const { newPassword, confirmPassword } = formData;
  const onchange = (e) => {
    // console.log(e.target.name)
    // console.log(e.target.value)
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    console.log(newPassword, confirmPassword, code);
    resetPassword(newPassword, confirmPassword, code, history);
  };

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div>
      <section className="login-main">
        <div className="container">
          <div className="login-inner">
            <div className="row">
              <div className="col-lg-12 col-12 ">
                <div className="right">
                  <div className="logo text-center"><img src="images/logo.png" alt="" /></div>
                  <h1 className>Password Recovery </h1>
                  <form onSubmit={e=>onSubmit(e)}>
                    <div className="row">
                      <div className="col-12 form-group position-relative">
                        <i className="fa fa-key  icon" />
                        <input type="password" className="form-control enter-input"placeholder="Enter New Password"  value={newPassword} name="newPassword"  placeholder="Enter new Password" onChange={(e)=>onchange(e)} required/>
                        <button className="view-btn position-absolute" > <i className="fa fa-eye-slash enter-icon right-icon" aria-hidden="true" /></button>
                      </div>  
                      <div className="col-12 form-group position-relative">
                        <i className="fa fa-key  icon" />
                        <input type="password" className="form-control enter-input" placeholder="Retype Password" value={confirmPassword} name="confirmPassword" placeholder="Retype Password" onChange={(e)=>onchange(e)} required/>
                        <button className="view-btn position-absolute"> <i className="fa fa-eye-slash enter-icon right-icon" aria-hidden="true" /></button>
                      </div>
                    </div>
                    <div className="flex-row">
                      <div className="d-flex justify-content-end">
                        {/*div class="">
                                        <label class="login-check">Remember Me
                                            <input type="checkbox" >
                                            <span class="checkmark"></span></label>
                                    </div*/}
                        {/* <div className> <a href="#" className="forgot" data-toggle="modal" data-target="#exampleModalCenter"> Forgot Password?</a> </div> */}
                      </div>	
                    </div>
                    <div className="row">
                      <div className="d-block col-12 text-center btn-bar">
                        <button type="submit" className="btn-default outline-btn">Update</button>
                        <Link to="/" className="btn-default fill-btn mt-1">Back To Sign In</Link>
                      </div>	
                    </div>							
                  </form>
                </div></div></div></div></div></section>
    </div>
  );
};
ResetPassword.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  passwordRecovery: PropTypes.bool,
  code: PropTypes.string,
};
const mapStateToProps = (state) => ({
  passwordRecovery: state.passwordRecovery,
  isAuthenticated: state.auth.isAuthenticated,
  code: state.auth.code,
});

export default connect(mapStateToProps, { resetPassword })(ResetPassword);

{
  //   <div className="container-fluid admin-login pl-0">
  //   <div className="row">
  //     <div className="col-5 pr-0">
  //       <div className="admin-login-inner d-flex align-items-center justify-content-center">
  //         <img src="images/logo-bg.png" alt="" className="img-fluid" />
  //       </div>
  //     </div>
  //     <div className="col-xl-4 col-lg-5 py-3 py-lg-0 col-md-6 my-auto ml-3">
  //       <div className>
  //         <div className="admin-login-card w-100 p-5">
  //           <div className="text-left">
  //             <h4 className="medium clr-orange">Password Recovery</h4>
  //             <form onSubmit={e=>onSubmit(e)}>
  //             <hr className="blue_line bck-orange" />
  //             <div className="md-form md-outline input-with-pre-icon">
  //               <i className="fas fa-key  input-prefix active" />
  //               <input type="text" id="prefixInside2" className="form-control py-2" placeholder="Enter New Password"  value={newPassword} name="newPassword"  placeholder="Enter new Password" onChange={(e)=>onchange(e)}/>
  //               <i className="fas fa-eye-slash hide-pass active" />
  //             </div>
  //             <div className="md-form md-outline input-with-pre-icon">
  //               <i className="fas fa-key  input-prefix active" />
  //               <input type="text" id="prefixInside2" className="form-control py-2" placeholder="Retype Password" value={confirmPassword} name="confirmPassword" placeholder="Retype Password" onChange={(e)=>onchange(e)} />
  //               <i className="fas fa-eye-slash hide-pass active" />
  //             </div>
  //             <a ><button type="submit" className="w-100 mt-2">Update</button></a>
  //             <div className="mt-3 text-center">
  //               <Link to="/login" className="medium clr_grey"><i className="fa fa-arrow-circle-left mr-2" /> Back
  //                 To Website</Link>
  //             </div>
  //             </form>
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   </div>
  // </div>
  // <ToastContainer autoClose={2000} />
}
