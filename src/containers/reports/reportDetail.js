import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import { getReportById } from '../../actions/reports';
import moment from 'moment';
import { get_ID } from '../../utils/commonFunctions';



 const ReportDetails = ({ getReportById, report: { Report },  match,history }) => {


  useEffect(() => {
   console.log(match.params.id)

  getReportById(match.params.id);


  }, [getReportById, match.params.id]);

     

        return (
          <Fragment>
            {Report === null ? (
              <Spinner />
            ) : (
                <div className="app-content content view user">
                <div className="content-wrapper">
                  <div className="content-body">
                    {/* Basic form layout section start */}
                    <section id="configuration" className="search view-cause">
                      <div className="row">
                        <div className="col-12">
                          <div className="card pad-20">
                            <div className="card-content collapse show">
                              <div className="card-body table-responsive card-dashboard">
                                <div className="row mt-2">
                                  <div className="col-12 d-block d-sm-flex justify-content-between">
                                    <div className="left">
                                      <a onClick={()=>history.goBack()}><h1><i className="fas fa-arrow-left" /> Report Details</h1></a>
                                    </div>
                                    <div className="right text-right">
                                    </div>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-lg-12 my-4">
                                    <div className="d-flex justify-content-between">
                                      <div className>
                                        <p><i className="fas fa-plane icon-yellow d-inline-block" /> <strong>ID: BAG-{get_ID(Report?.delivery?._id)}</strong></p>
                                      </div>
                                      <div className>
                                      </div>
                                    </div>
                                  </div>
                                  <hr />
                                </div>
                                <div className="row">
                                  <div className="col-lg-12">
                                    <h1>User Reporting</h1>
                                  </div>
                                </div>
                                <div className="row my-1">
                                  <div className="col-lg-3 my-1">
                                    <p><strong>First Name</strong></p>
                                    <p>{Report?.delivery?.user?.firstname}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>Last Name</strong></p>
                                    <p>{Report?.delivery?.user?.lastname}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>Email Address</strong></p>
                                    <p>{Report?.delivery?.user?.email}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>Report Date</strong></p>
                                    <p>{moment(Report?.createdAt).format('LL')}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>To </strong></p>
                                    <p>{Report?.delivery?.to}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>From </strong></p>
                                    <p>{Report?.delivery?.from}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>Delivery Status </strong></p>
                                    <p>{Report?.delivery?.status}</p>
                                  </div>
                                  <div className="col-lg-3 my-1">
                                    <p><strong>Report Reason </strong></p>
                                    <p>{Report?.reportReason}</p>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-lg-12">
                                    <p>{Report?.delivery?.description}.</p>
                                  </div>
                                </div>
                                <div className="row my-2">
                                  <div className="col-lg-2">
                                    <p><strong>Total Cost</strong></p>
                                    <p>{Report?.delivery?.total_cost}</p>
                                  </div>
                                  <div className="col-lg-2">
                                    <p><strong>Fed-Ex Cost </strong></p>
                                    <p>${Report?.delivery?.fed_ex_cost}</p>
                                  </div>
                                  <div className="col-lg-2">
                                    <p><strong>Status </strong></p>
                                    <p>{Report?.report_status}</p>
                                  </div>
                                </div>
                                <div className="row my-3">
                                  <div className="col-lg-12">
                                    <a href="#" className="btn btn-dblue" data-toggle="modal" data-target="#approve">Refund</a>
                                    <a href="#" className="btn btn-red">Reject</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
              

              
            )}





        <div className="modal fade" id="approve" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center my-4 mx-3">
                <img src="images/question-mark.png" className="img-fluid" />
                <h2 className="bold">Alert</h2>
                <p>Are you sure you want to approve this request?</p>
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <a href="login.php">
                      <button type="button" className="btn-black bs-1" data-dismiss="modal">No</button>
                      <button type="button" className="btn-dblue bs-1" data-dismiss="modal" data-toggle="modal" data-target="#confirm">Yes</button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* MODAL SUCCESS END  */}
        {/* MODAL SUCCESS START  */}
        <div className="modal fade" id="confirm" role="dialog">
          <div className="modal-dialog modal-dialog-centered">
            {/* Modal content*/}
            <div className="modal-content modal-cstm ">
              <div className="modal-body modal-bodyCstm text-center my-4 mx-3">
                <img src="images/icon-success.png" className="img-fluid" />
                <h2 className="bold">Confirmation</h2>
                <p>Delivery Request has been accepted</p>
                <div className="row">
                  <div className="col-lg-12 text-center">
                    <button type="button" className="btn-dblue bs-1" data-dismiss="modal">OK</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
          </Fragment>
        );
      };
    
ReportDetails.propTypes = {
  getReportById: PropTypes.func.isRequired,
  report: PropTypes.object.isRequired,

};

const mapStateToProps = (state) => ({
  report: state.reports
});

export default connect(mapStateToProps, { getReportById })(ReportDetails);
