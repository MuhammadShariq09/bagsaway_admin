import moment from "moment";
import React, { useEffect, useRef } from "react";
import "./chatBubble.css";

export default function ChatBubble({
  messages,
  user,
  selectedUser,
  baseURL,
  msg,
}) {
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  useEffect(() => {
    scrollToBottom();
  }, [msg]);

  if (messages?.length === 0) {
    return <h1>No Messages With This User Yet!</h1>;
  }

  return (
    <section className="msger">
      <main className="msger-chat">
        {messages?.map((msg) => (
          <div
            className={`msg ${
              msg.fromid == selectedUser?.id ? "left-msg" : "right-msg"
            }`}
          >
            <div
              className="msg-img"
              style={{
                backgroundImage: `url(${
                  msg.fromid == selectedUser?.id
                    ? `${baseURL}/${selectedUser?.image}`
                    : `${baseURL}/${user?.image}`
                })`,
              }}
            />
            <div className="msg-bubble">
              <div className="msg-info">
                <div className="msg-info-name">
                  {msg.fromid == selectedUser?.id
                    ? `${selectedUser?.name}`
                    : `${user?.firstname} ${user?.lastname}`}
                </div>
                <div className="msg-info-time">
                  {moment(msg?.created_at_server).format("hh:mm DD-MM")}
                </div>
              </div>
              <div className="msg-text">{msg?.content}</div>
            </div>
          </div>
        ))}
        {msg?.toid && (
          <div className={`msg ${"left-msg"}`}>
            <div
              className="msg-img"
              style={{
                backgroundImage: `url(${baseURL}/${selectedUser?.image})`,
              }}
            />
            <div className="msg-bubble">
              <div className="msg-info">
                <div className="msg-info-name">{selectedUser?.name}</div>
                <div className="msg-info-time">
                  {moment(msg?.created_at_server).format("hh:mm DD-MM")}
                </div>
              </div>
              <div className="msg-text">{msg?.content}</div>
            </div>
          </div>
        )}
        <div ref={messagesEndRef} />
      </main>
    </section>
  );
}
