import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./chat.css";

import ChatBubble from "./chatBubble";

import { baseURL } from "../../utils/index";
import axios from "axios";
import moment from "moment";
import { socket } from "../../App";

function Chat({ auth: { user } }) {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [token, setToken] = useState("");
  const [selectedData, setSelectedData] = useState(null);
  const [message, setMessage] = useState("");
  const [soaId, setSoaId] = useState("");
  const [newMessage, setNewMessage] = useState("");

  const handleLogin = () => {
    axios({
      url: "https://dev28.onlinetestingserver.com/soachatapi/api/login",
      method: "POST",
      data: {
        appid: "b5b4227516d35f0d0af273a2812119e3",
        secret_key:
          "$2y$10$Uuy.WDtyeoqfmnI4XDx6..94pDu1ctXSZTI6AW9DtNYxJhaEONfwu",
        id: user?._id,
      },
    })
      .then((res) => {
        console.log("USER LOGGED INTO SOA CHAT SYSTEM");
        setToken(res.data.token);
        setSoaId(res.data?.data?.id);
      })
      .catch((err) => {
        console.log("ERROR LOGGING INTO SOA CHAT SYSTEM");
        console.log(err);
      });
  };

  const handleGetChatMessages = () => {
    axios({
      url: "https://dev28.onlinetestingserver.com/soachatapi/api/chat/messages?page=1",
      method: "POST",
      data: {
        toid: selectedUser?.id,
        group_id: "",
      },
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        let data = res?.data?.data;
        if (data?.data?.length > 0) {
          data.data.reverse();
        }
        setSelectedData(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getAllUsers = () => {
    axios({
      url: "https://dev28.onlinetestingserver.com/soachatapi/api/user/get-my-friend-list-new",
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        setUsers(res.data?.data?.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleSendMessage = () => {
    if (message)
      axios({
        url: "https://dev28.onlinetestingserver.com/soachatapi/api/chat/save/messages",
        method: "POST",
        data: {
          toid: selectedUser?.id,
          group_id: "",
          content: message,
          created_at: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
        },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => {
          setMessage("");
          handleGetChatMessages();
          getAllUsers();
          const data = {
            toid: selectedUser?.id.toString(),
            fromId: soaId,
            group_id: "",
            content: message,
            created_at: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
            created_at_server: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
          };
          socket.emit("soa_chat", data);
          setNewMessage(null);
        })
        .catch((err) => {
          console.log(err);
          setMessage("");
          handleGetChatMessages();
          getAllUsers();
          const data = {
            toid: selectedUser?.id,
            group_id: "",
            content: message,
            created_at: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
            created_at_server: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
          };
          socket.emit("soa_chat", data);
          setNewMessage(null);
        });
  };

  useEffect(() => {
    socket.on("soa_chat", (data) => {
      console.log(data?.fromId, selectedUser, data?.fromId == selectedUser?.id);
      if (data?.toid == soaId && data?.fromId == selectedUser?.id) {
        setNewMessage(data);
      }
    });
  }, [soaId, selectedUser]);

  useEffect(() => {
    if (newMessage?.toid) {
      getAllUsers();
    }
  }, [newMessage]);

  useEffect(() => {
    if (user?._id) {
      handleLogin();
    }
  }, [user]);

  useEffect(() => {
    if (token) {
      getAllUsers();
    }
  }, [token]);

  useEffect(() => {
    if (selectedUser?.id) handleGetChatMessages();
  }, [selectedUser]);

  if (!token) {
    return <h1>LOADING....</h1>;
  }

  return (
    <div className="app-content user-management content main-chat-box">
      <div className="content-wrapper">
        <div className="content-body">
          <div className="wrapper">
            <div className="search">
              <i className="fa fa-search" />
              <input
                type="text"
                name="search"
                id="search"
                placeholder="Search users..."
              />
            </div>
            <div className="header">
              <h1>{selectedUser ? `${selectedUser?.name}` : "inbox"}</h1>
            </div>
            <div className="usersList">
              {users?.map((user) => (
                <a
                  className="profileContainer"
                  data-uid={1}
                  data-username="username"
                  data-activity={1}
                  data-last={1}
                  key={user?.id}
                  onClick={() => {
                    if (user?.id != selectedUser?.id) {
                      setSelectedUser(user);
                      setSelectedData(null);
                      setNewMessage(null);
                    }
                  }}
                >
                  <div className="profileAvatar">
                    <img
                      src={
                        user?.image
                          ? user?.image
                          : "https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png"
                      }
                      onError={(e) => {
                        e.target.onerror = null;
                        e.target.src =
                          "https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png";
                      }}
                      alt="Username"
                    />
                  </div>
                  <div className="profileHolder">
                    <div className="profileName">{user?.name}</div>
                    <div className="profileLastMsg">{`${user?.recent_message?.substring(
                      0,
                      25
                    )}....`}</div>
                    {user?.unread_messages_count !== 0 && (
                      <a href="#" class="notification">
                        <span className="badge">
                          {user?.unread_messages_count}
                        </span>
                      </a>
                    )}
                  </div>
                </a>
              ))}
            </div>
            <div className="conversation">
              <div className="conversationContainer welcome">
                {selectedUser ? (
                  selectedData ? (
                    <ChatBubble
                      messages={selectedData?.data}
                      user={user}
                      selectedUser={selectedUser}
                      baseURL={baseURL}
                      msg={newMessage}
                    />
                  ) : (
                    <h1>LOADING....</h1>
                  )
                ) : (
                  <div className="welcomeMessage" style={{ width: "100%" }}>
                    <h3>Welcome to your inbox!</h3>
                    <p>Choose users from your menu and chat</p>
                  </div>
                )}
              </div>
              <div className="sendMessageContainer">
                <div className="sendContentContainer">
                  <textarea
                    id="sendContent"
                    placeholder="Write your message here"
                    rows={1}
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                  />
                </div>
                <div className="sendButtonContainer">
                  <button
                    type="button"
                    id="sendButton"
                    onClick={handleSendMessage}
                  >
                    <i className="fa fa-lg fa-send" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Chat.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(Chat);
