import React, { Fragment } from 'react';
import spinner from './spinner.gif';

export default () => (
  <Fragment>
   <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "calc(100vh - 60px)",
      background: "transparent",
    }}
  >
   <div className="loader-container" >
     <div className="loader" />
     <span className="loading-text">Loading...</span>
   </div>
  </div>
  </Fragment>
);
