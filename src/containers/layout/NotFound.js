import React, { Fragment } from 'react';

const NotFound = () => {
  return (
    <Fragment>
           <div className="app-content content view user">
        <div className="content-wrapper">
          <div className="content-body">
            {/* Basic form layout section start */}
            <section id="configuration" className="search view-cause">
              <div className="row">
                <div className="col-12">
                  <div className="card pad-20">
                    <div className="card-content collapse show">
                      <div className="card-body table-responsive card-dashboard py-5">
                        <div className="row my-5">
                          <div className="col-lg-12 text-center my-5">
                            <h1 className="fc-yellow">OOPS!</h1>
                            <h1>PAGE NOT FOUND</h1>
                            <img src="images/404.png" className="img-fluid mt-2" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default NotFound;
