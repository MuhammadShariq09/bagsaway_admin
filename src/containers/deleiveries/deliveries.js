import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Spinner from "../layout/Spinner";
import { getDeliveries } from "../../actions/deliveriesActions";
import { Link } from "react-router-dom";

import Pagination from "../../Components/Pagination";
import "react-datepicker/dist/react-datepicker.css";
import TableControls from "../../Components/TableControls";
import TableHeaders from "../../Components/TableHeaders";
import TableRow from "../../Components/TableRow";
import moment from "moment";
import "react-toastify/dist/ReactToastify.css";
import { get_ID } from "../../utils/commonFunctions";
import ViewLoader from "../../Components/ViewLoader";
const Deleiveries = ({
  getDeliveries,
  SortAction,
  // UpdateUserStatus,
  delivery: { Deliverys, loading },
  loader : {Loader},
  history,
}) => {
  const [currentID, setCurrentID] = useState("");
  const [Selection, setSelection] = useState("");
  const [blockStatus, setBlockStatus] = useState(1);
  const [OrderState, setOrderState] = useState(1);
  const [page, setPage] = useState(1);
  const [limit, setlimit] = useState(5);
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [keyword, setKeyword] = useState("");

  const blockToggleUser = (id, status) => {
    //  alert(id,""+status)
    setCurrentID(id);

    setBlockStatus(status);

    //  window.jQuery('#userBlocked').modal('show');
  };

  useEffect(() => {
    // alert(Selection)
    getDeliveries(page, limit, Selection, from, to, keyword);
  }, [getDeliveries, page, limit, Selection, from, to, keyword]);

  const toggleOrder = () => {
    const currentState = OrderState === 1 ? -1 : 1;
    setOrderState(currentState);
    return OrderState;
  };
  const handleSelection = (e) => {
    // const currentState = Selection === 1 ? 0 : 1
    setSelection(e.target.value);
    return Selection;
  };
  const handleLimit = (e) => {
    // alert(e.target.value)
    let value = e.target.value;

    setlimit(value);
  };

  const handlePageClick = (data) => {
    let selected = data.selected + 1;
    // console.log(selected)
    setPage(selected);
  };
  const handleKeyword = (e) => {
    console.log(e.target.value)
    let value = e.target.value;

    setKeyword(value);
  };

  return (
    <Fragment>
      {loading ? (
        <ViewLoader fontSize="100px" />
      ) : (
        <>
          <div className="app-content content view user">
            <div className="content-wrapper">
              <div className="content-body">
                {/* Basic form layout section start */}
                <section id="configuration" className="search view-cause">
                  <div className="row">
                    <div className="col-12">
                      <div className="card pad-20">
                        <div className="card-content collapse show">
                          <div className="card-body table-responsive card-dashboard">
                            <div className="row mt-2">
                              <div className="col-12 d-block d-sm-flex justify-content-between">
                                <div className="left">
                                  <h1>Deliveries</h1>
                                </div>
                                <div className="right text-right">
                                  {/* <a to="">Add User</a> */}
                                  <div class="all-select">
                                    <select
                                      name=""
                                      id=""
                                      class="form-control filter"
                                      onChange={(e) => handleSelection(e)}
                                    >
                                      <option value="">Filter By Status</option>
                                      <option value="">All</option>
                                      <option value="PENDING">Pending</option>
                                      <option value="APPROVED">Approved</option>
                                      <option value="REJECTED">Rejected</option>
                                      <option value="DELIVERED">
                                        Delivered
                                      </option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="row maain-tabble mt-1">
                              <div
                                id="DataTables_Table_0_wrapper"
                                className="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"
                              >
                                <TableControls
                                  handleLimit={handleLimit}
                                  handleKeyword={handleKeyword}
                                />
                              {Loader?
                               <ViewLoader  fontSize="100px"/>:
                                <div className="row">
                                  <div className="col-sm-12">
                                    <table
                                      className="table table-striped table-bordered zero-configuration dataTable no-footer"
                                      id="DataTables_Table_0"
                                      role="grid"
                                      aria-describedby="DataTables_Table_0_info"
                                    >
                                      <TableHeaders
                                        data={[
                                          "s.no",
                                          "ID.no",
                                          "first name",
                                          "last name",
                                          "status",
                                          "total Cost",
                                          "Fed-Ex Cost",
                                          "Date",

                                          "action",
                                        ]}
                                      />

                                      <tbody>
                                        {Deliverys &&
                                        Object.keys(Deliverys).length > 1 ? (
                                          Deliverys.data.map((item, index) => (
                                            <tr role="row" className="odd">
                                              <TableRow
                                                data={[
                                                  index + 1,
                                                  get_ID(item?._id),
                                                  item?.user?.firstname,
                                                  item?.user?.lastname,
                                                  item?.status,
                                                  item?.total_cost,
                                                  item?.fed_ex_cost,

                                                  moment(
                                                    item?.createdAt
                                                  ).format("LL"),

                                                  <div className="btn-group mr-1 mb-1">
                                                    <button
                                                      type="button"
                                                      className="btn  btn-drop-table btn-sm"
                                                      data-toggle="dropdown"
                                                      aria-haspopup="true"
                                                      aria-expanded="false"
                                                    >
                                                      {" "}
                                                      <i className="fa fa-ellipsis-v" />
                                                    </button>
                                                    <div
                                                      className="dropdown-menu dropdown-menu-right"
                                                      x-placement="bottom-start"
                                                      style={{
                                                        position: "absolute",
                                                        transform:
                                                          "translate3d(4px, 23px, 0px)",
                                                        top: "0px",
                                                        left: "0px",
                                                        willChange: "transform",
                                                      }}
                                                    >
                                                      <Link
                                                        className="dropdown-item"
                                                        to={`/delivery-detail/${item._id}`}
                                                      >
                                                        <i className="fa fa-eye" />
                                                        Details
                                                      </Link>
                                                    </div>
                                                  </div>,
                                                  ,
                                                  "action",
                                                ]}
                                              ></TableRow>
                                            </tr>
                                          ))
                                        ) : (
                                          <tr class="odd">
                                            <td
                                              valign="top"
                                              colspan="8"
                                              class="dataTables_empty"
                                            >
                                              No matching records found
                                            </td>
                                          </tr>
                                        )}
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
}
                                <Pagination
                                  total={Deliverys && Deliverys.total}
                                  perPage={Deliverys && Deliverys.perPage}
                                  handlePageClick={handlePageClick}
                                />
                              </div>
     
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </>
      )}
    </Fragment>
  );
};

Deleiveries.propTypes = {
  getDeliveries: PropTypes.func.isRequired,

  UpdateUserStatus: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  delivery: state.delivery,
  loader :state.loader
});

export default connect(mapStateToProps, {
  getDeliveries,
  // UpdateUserStatus,
  // SortAction,
})(Deleiveries);
