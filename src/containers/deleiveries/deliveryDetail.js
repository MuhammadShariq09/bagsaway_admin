import React, { useEffect, Fragment, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import { Link, withRouter } from "react-router-dom";

import {
  getDeliveyByID,
  UpdateDeliveryStatus,
} from "../../actions/deliveriesActions";
import { ToastContainer } from "react-toastify";
import { get_ID } from "../../utils/commonFunctions";

const UserDetails = ({
  getDeliveyByID,
  UpdateDeliveryStatus,
  updateUser,
  delivery: { Delivery, loading },
  auth,
  match,
  history,
}) => {
  const [status, setStatus] = useState("");

  useEffect(() => {
    getDeliveyByID(match.params.id);
  }, [getDeliveyByID, match.params.id]);

  const onSubmit = (e) => {
    e.preventDefault();
    // console.log("firstname,lastname")

    // updateUser(match.params.id,firstname,lastname,phone_no,history)
  };

  return (
    <Fragment>
      {Delivery !== null || Delivery != undefined ? (
        <Fragment>
          <div className="app-content content view user">
            <div className="content-wrapper">
              <div className="content-body">
                {/* Basic form layout section start */}
                <section id="configuration" className="search view-cause">
                  <div className="row">
                    <div className="col-12">
                      <div className="card pad-20">
                        <div className="card-content collapse show">
                          <div className="card-body table-responsive card-dashboard">
                            <div className="row mt-2">
                              <div className="col-12 d-block d-sm-flex justify-content-between">
                                <div className="left">
                                  <a onClick={() => history.goBack()}>
                                    <h1>
                                      <i className="fas fa-arrow-left" />{" "}
                                      Deliveries
                                    </h1>
                                  </a>
                                </div>
                                <div className="right text-right"></div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-lg-12 my-4">
                                <div className="d-flex justify-content-between">
                                  <div className>
                                    <p>
                                      <i className="fas fa-plane icon-yellow d-inline-block" />{" "}

                                      <strong>
                                        ID:{get_ID(Delivery?._id)}
                                      </strong>
                                    

                                    
                          
                                    </p>
                                  </div>
                                  <div className>
                                    <p>
                                      <strong>
                                        {moment(Delivery?.date).format("LL")}
                                      </strong>
                                    </p>
                                  </div>
                                </div>
                              </div>
{
  Delivery?.status =="APPROVED"?

                              <div className="col-lg-12 mb-4 text-center">
                                <ul className="order-track">
                                  <li className="active">
                                    <span className="text">Payment</span>
                                    <span className="iconStep">
                                      <i className="fas fa-check" />
                                    </span>
                                  </li>
                                  <li>
                                    <span className="text">Picked Up</span>
                                  </li>
                                  <li>
                                    <span className="text">Shipped</span>
                                  </li>
                                  <li>
                                    <span className="text">Delivered</span>
                                  </li>
                                  <li className="lastChild">
                                    <span className="text">Recieved</span>
                                  </li>
                                </ul>
                              </div> :""
                              
}
                             

                              <hr />
                            </div>
                            <div className="row">
                              <div className="col-lg-12">
                                <h1>User Details</h1>
                              </div>
                            </div>
                            <div className="row my-3">
                              <div className="col-lg-3">
                                <p>
                                  <strong>First Name</strong>
                                </p>
                                <p>{Delivery?.user?.firstname}</p>
                              </div>
                              <div className="col-lg-3">
                                <p>
                                  <strong>Last Name</strong>
                                </p>
                                <p>{Delivery?.user?.lastname}</p>
                              </div>
                              <div className="col-lg-3">
                                <p>
                                  <strong>Email Address</strong>
                                </p>
                                <p>{Delivery?.user?.email}</p>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-lg-12">
                                <h1>Delivery Details</h1>
                              </div>
                            </div>
                            <div className="row mt-3">
                              <div className="col-lg-2">
                                <p>
                                  <strong>To </strong>
                                </p>
                                <p>{Delivery?.to?.street}</p>

                              </div>
                              <div className="col-lg-2">
                                <p>
                                  <strong>From </strong>
                                </p>
                                <p>{Delivery?.from?.street}</p>
                              </div>
                              <div className="col-lg-2">
                                <p>
                                  <strong>Pickup Date </strong>
                                </p>
                                <p>
                                  {moment(Delivery?.pickup_date).format("LL")}
                                </p>
                              </div>
                              <div className="col-lg-2">
                                <p>
                                  <strong>Luggage Weight </strong>
                                </p>
                                <p>{Delivery?.luggage_weight} KG</p>
                              </div>
                            </div>
                            <div className="row">
                              <div className="d-lg-flex">
                                <div className="m-1">
                                  <img
                                    src="images/luggage.png"
                                    className="img-fluid"
                                  />
                                </div>
                                <div className="m-1">
                                  <img
                                    src="images/luggage.png"
                                    className="img-fluid"
                                  />
                                </div>
                                <div className="m-1">
                                  <img
                                    src="images/luggage.png"
                                    className="img-fluid"
                                  />
                                </div>
                                <div className="m-1">
                                  <img
                                    src="images/luggage.png"
                                    className="img-fluid"
                                  />
                                </div>
                                <div className="m-1">
                                  <img
                                    src="images/luggage.png"
                                    className="img-fluid"
                                  />
                                </div>
                              </div>
                              <div className="col-lg-12">
                                <p>
                                  <strong>Additional Note</strong>
                                </p>
                                <p>{Delivery?.description}.</p>
                              </div>
                            </div>
                            <div className="row mt-3">
                              <div className="col-lg-12">
                                <h1>Payment &amp; Status</h1>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-lg-2">
                                <p>
                                  <strong>Total Cost</strong>
                                </p>
                                <p>${Delivery?.total_cost}</p>
                              </div>
                              <div className="col-lg-2">
                                <p>
                                  <strong>Fed-Ex Cost </strong>
                                </p>
                                <p>${Delivery?.fed_ex_cost}</p>
                              </div>
                              <div className="col-lg-2">
                                <p>
                                  <strong>Status </strong>
                                </p>
                                <p>{Delivery?.payment_status}</p>
                              </div>
                            </div>
                            <div className="row my-3">
                              <div className="col-lg-12">
                                <a
                                  onClick={() => setStatus("APPROVED")}
                                  className={`btn btn-green ${
                                    Delivery.status === "PENDING"
                                      ? ""
                                      : "hidden"
                                  } `}
                                  data-toggle="modal"
                                  data-target="#approve"
                                >
                                  Approve
                                </a>
                                <a
                                  onClick={() => setStatus("REJECTED")}
                                  className={`btn btn-red ${
                                    Delivery.status === "PENDING"
                                      ? ""
                                      : "hidden"
                                  } `}
                                  data-toggle="modal"
                                  data-target="#approve"
                                >
                                  Reject
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <div>
            {/* MODAL SUCCESS START  */}
            <div className="modal fade" id="approve" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                {/* Modal content*/}
                <div className="modal-content modal-cstm ">
                  <div className="modal-body modal-bodyCstm text-center my-4 mx-3">
                    <img src="images/question-mark.png" className="img-fluid" />
                    <h2 className="bold">Alert</h2>
                    <p>
                      Are you sure you want to{" "}
                      {status == "APPROVED" ? "accept" : "reject"} this
                      Delivery?
                    </p>
                    <div className="row">
                      <div className="col-lg-12 text-center">
                        <a>
                          <button
                            type="button"
                            className="btn-black bs-1"
                            data-dismiss="modal"
                          >
                            No
                          </button>
                          <button
                            type="button"
                            onClick={() =>
                              UpdateDeliveryStatus(
                                Delivery._id,
                                status,
                                history
                              )
                            }
                            className="btn-dblue bs-1"
                          >
                            Yes
                          </button>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* MODAL SUCCESS END  */}
            {/* MODAL SUCCESS START  */}
            <div className="modal fade" id="confirm" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                {/* Modal content*/}
                <div className="modal-content modal-cstm ">
                  <div className="modal-body modal-bodyCstm text-center my-4 mx-3">
                    <img src="images/icon-success.png" className="img-fluid" />
                    <h2 className="bold">Confirmation</h2>
                    <p>Delivery Request has been accepted</p>
                    <div className="row">
                      <div className="col-lg-12 text-center">
                        <button
                          type="button"
                          className="btn-dblue bs-1"
                          data-dismiss="modal"
                        >
                          OK
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      ) : (
        "<h1>user Details doesonot found</h1>"
      )}
    </Fragment>
  );
};

UserDetails.propTypes = {
  updateProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  delivery: state.delivery,
});

export default connect(mapStateToProps, {
  getDeliveyByID,
  UpdateDeliveryStatus,
})(withRouter(UserDetails));
