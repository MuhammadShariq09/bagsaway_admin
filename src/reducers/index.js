import { combineReducers } from 'redux';
import auth from './authReducer';
import profile from './profileReducer';
import dashboard from './dashboardReducer'
import feedback from './feedbackReducer'
import delivery from './deliveryReducer'
import reports from './reports'
import loader from './loaderReducer'

export default combineReducers({
  auth,
  profile,
  dashboard,
  feedback,
  delivery,
  reports,
  loader
});
