import { getFeedbackById } from "../actions/feedbackAction";
import {
  GET_DELIVERY_BY_ID,
  GET_DELIVERYS,
  DELIVERYS_ERROR,
} from "../actions/types";

const initialState = {
  Deliverys: [],
  Delivery: null,
  loading: true,
  error: {},
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_DELIVERYS:
      return {
        ...state,
        Deliverys: payload,
        loading: false,
      };
    case GET_DELIVERY_BY_ID: {
      return {
        ...state,
        Delivery: payload,
        loading: false,
      };
    }
    case DELIVERYS_ERROR:{
        return{
            ...state,
            Deliverys:[],
            Delivery:[],
            loading:false
        }
    }
    default:
      return state;
  }
}
